#!/bin/bash

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
BINARY=$ROOT_DIR/bin/droned
SSH_TARGET=dronepi
DEPLOY_PATH=drone/bin

if [ ! -f $BINARY ]; then
    echo "Binary not found. Has the project been built?" >&2
    exit 1
fi

ssh $SSH_TARGET "mkdir -p $DEPLOY_PATH"
scp $BINARY $SSH_TARGET:$DEPLOY_PATH
