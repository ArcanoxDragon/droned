#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root"
    exit
fi

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Update/upgrade all packages
apt update && apt upgrade -y

# Install required packages for cross compilation
apt install -y build-essential gawk git texinfo bison file wget

# Install NodeJS if it's not present
if ! (which node > /dev/null); then
    echo "NodeJS not detected, installing..."
    curl -fsSL https://deb.nodesource.com/setup_15.x | bash -
    apt install -y nodejs
fi

# Ensure global Node packages are installed
npm i -g yarn typescript

# Set up cross-compilation environment
if ! [[ -d /opt/cross-pi-gcc ]]; then
    echo "Setting up Pi cross-compiler"
    mkdir -p $ROOT_DIR/tmp
    mkdir -p /opt/cross-pi-gcc/sysroot
    pushd $ROOT_DIR/tmp >/dev/null
    wget https://github.com/Pro/raspi-toolchain/releases/latest/download/raspi-toolchain.tar.gz
    tar vxfz raspi-toolchain.tar.gz --strip-components=1 -C /opt
    popd >/dev/null
    rm -rf $ROOT_DIR/tmp
    
    echo $'You must run ./sync-pi-rootfs.sh once the "dronepi" SSH host is set up to fetch all the required libs from the Pi\'s rootfs'
fi

echo "Setup complete!"

cat << EOM
If you have not done so already, you should add a "dronepi" SSH host to ~/.ssh/config:

Host dronepi
    HostName <drone IP address>
    User pi
    IdentityFile ~/.ssh/id_rsa
EOM
