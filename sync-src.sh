#!/bin/bash

SSH_TARGET=dronepi
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SOURCE_PATH=$ROOT_DIR/src
$DEPLOY_PATH=drone

ssh $SSH_TARGET "mkdir -p $DEPLOY_PATH"
rsync -vas --progress $SOURCE_PATH $SSH_TARGET:$DEPLOY_PATH
