BIN_DIR			= bin
SRCS_DIR		= src
OBJS_DIR		= obj
ASM_DIR			= asm

LOCAL_HEADERS	:= $(shell find $(SRCS_DIR) -name *.h)
LOCAL_HEADERS	+= $(shell find $(SRCS_DIR) -name *.hpp)
LOCAL_SRCS		:= $(shell find $(SRCS_DIR) -name *.cpp)
LOCAL_OBJS		:= $(subst .cpp,.o,$(LOCAL_SRCS))
LOCAL_OBJS		:= $(subst $(SRCS_DIR)/,$(OBJS_DIR)/,$(LOCAL_OBJS))
LOCAL_ASMS		:= $(subst .cpp,.s,$(LOCAL_SRCS))
LOCAL_ASMS		:= $(subst $(SRCS_DIR)/,$(ASM_DIR)/,$(LOCAL_ASMS))

TOOLCHAIN		:= /opt/cross-pi-gcc
SYSROOT			:= $(TOOLCHAIN)/sysroot
ARCH			:= arm
CROSS_COMPILE	:= $(TOOLCHAIN)/bin/arm-linux-gnueabihf-

CC				:= $(CROSS_COMPILE)gcc
CXX				:= $(CROSS_COMPILE)g++
LD				:= $(CROSS_COMPILE)ld
RM				:= rm -f
MKDIR			:= mkdir -p

LDLIBS			:= -lwiringPi -lSDL2
INCLUDE_DIRS	:= -isystem$(SYSROOT)/usr/include/arm-linux-gnueabihf -I$(SYSROOT)/usr/include/ -I$(SYSROOT)/usr/local/include/
CPPFLAGS		:= --sysroot=$(SYSROOT) -fpermissive -std=gnu++14 $(INCLUDE_DIRS)
LDFLAGS			:= --sysroot=$(SYSROOT) -L$(SYSROOT)/usr/local/lib -L$(SYSROOT)/opt/vc/lib
TARGET			:= $(BIN_DIR)/droned

ifeq ($(DEBUG),1)
	CPPFLAGS	+= -g
else
	CPPFLAGS	+= -O2
endif

ifeq ($(VERBOSE),1)
	CPPFLAGS	+= -DVERBOSE
endif

all: $(TARGET)

asm: $(LOCAL_ASMS)

$(TARGET): $(LOCAL_OBJS)
	@$(MKDIR) $(@D)
	$(CXX) $(LDFLAGS) -o $(TARGET) $(LOCAL_OBJS) $(LDLIBS)
	
$(OBJS_DIR)/%.o: $(SRCS_DIR)/%.cpp $(LOCAL_HEADERS)
	@$(MKDIR) $(@D)
	$(CXX) $(CPPFLAGS) -c $< -o $@
	
$(ASM_DIR)/%.s: $(SRCS_DIR)/%.cpp $(LOCAL_HEADERS)
	@$(MKDIR) $(@D)
	$(CXX) $(CPPFLAGS) -S -c $< -o $@
	
clean:
	$(RM) -r $(OBJS_DIR)/**
	$(RM) -r $(ASM_DIR)/**