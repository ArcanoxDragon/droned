import { Angles, Diagnostics, PidParams } from "@/ipc/models";

export function writePidParams(pidParams: PidParams, buffer: Buffer, offset: number): number {
    const TotalLength = 28; // uint32 + 3 * double

    if (buffer.length < TotalLength + offset)
        throw new Error("Buffer too small to write PID params");

    offset = buffer.writeInt32LE(pidParams.pidId, offset);
    offset = buffer.writeDoubleLE(pidParams.p, offset);
    offset = buffer.writeDoubleLE(pidParams.i, offset);
    offset = buffer.writeDoubleLE(pidParams.d, offset);

    return offset;
}

export function readPidParams(buffer: Buffer, offset: number): PidParams {
    // 4 bytes @ offset + 0:  uint32 pidId
    // 8 bytes @ offset + 4:  double p
    // 8 bytes @ offset + 12: double i
    // 8 bytes @ offset + 20: double d
    // total: 28 bytes

    const TotalLength = 28; // int32 + 3 * double

    if (buffer.length < TotalLength + offset)
        throw new Error("Buffer too small to read PID params");

    const pidId = buffer.readInt32LE(offset);
    const p = buffer.readDoubleLE(offset + 4);
    const i = buffer.readDoubleLE(offset + 12);
    const d = buffer.readDoubleLE(offset + 20);

    return { pidId, p, i, d };
}

export function readDiagnostics(buffer: Buffer, offset: number): Diagnostics {
    // 8 bytes @ offset + 0: double orientation.pitch
    // 8 bytes @ offset + 8: double orientation.roll
    // 8 bytes @ offset + 16: double orientation.heading
    // 8 bytes @ offset + 24: double targetOrientation.pitch
    // 8 bytes @ offset + 32: double targetOrientation.roll
    // 8 bytes @ offset + 40: double targetOrientation.heading
    // 4 bytes @ offset + 48: uint32 pitchPid.id
    // 8 bytes @ offset + 52: double pitchPid.p
    // 8 bytes @ offset + 60: double pitchPid.i
    // 8 bytes @ offset + 68: double pitchPid.d
    // 4 bytes @ offset + 76: uint32 rollPid.id
    // 8 bytes @ offset + 80: double rollPid.p
    // 8 bytes @ offset + 88: double rollPid.i
    // 8 bytes @ offset + 96: double rollPid.d
    // 4 bytes @ offset + 104: uint32 yawPid.id
    // 8 bytes @ offset + 108: double yawPid.p
    // 8 bytes @ offset + 116: double yawPid.i
    // 8 bytes @ offset + 124: double yawPid.d
    // 8 bytes @ offset + 132: double pitchControl
    // 8 bytes @ offset + 140: double rollControl
    // 8 bytes @ offset + 148: double yawControl
    // total: 156 bytes

    const TotalLength = 156;

    if (buffer.length < TotalLength + offset)
        throw new Error("Buffer too small to read diagnostic data");
    
    function readNextDouble() {
        const value = buffer.readDoubleLE(offset);
        
        offset += 8; // double
        
        return value;
    }
    
    function readNextPidParams() {
        const value = readPidParams(buffer, offset);
        
        offset += 28; // sizeof(PidParams), see above
        
        return value;
    }
    
    const orientation: Angles = {
        pitch: readNextDouble(),
        roll: readNextDouble(),
        heading: readNextDouble(),
    };
    const targetOrientation: Angles = {
        pitch: readNextDouble(),
        roll: readNextDouble(),
        heading: readNextDouble(),
    };
    const pitchPid = readNextPidParams();
    const rollPid = readNextPidParams();
    const yawPid = readNextPidParams();
    const pitchControl = readNextDouble();
    const rollControl = readNextDouble();
    const yawControl = readNextDouble();
    
    return {
        orientation,
        targetOrientation,
        pitchPid,
        rollPid,
        yawPid,
        pitchControl,
        rollControl,
        yawControl,
    };
}