import dgram from "dgram";

import { Diagnostics, PidParams } from "@/ipc/models";
import { readDiagnostics, readPidParams, writePidParams } from "@/ipc/data";

const SocketPort = 9888;
const RequestTimeout = 5000; // 5 seconds
// const RequestTimeout = 1000; // 1 second
const MessageIds = {
    SetPidParam: 1,
    GetPidParam: 2,
    PidParamData: 3,
    GetDiagnostics: 4,
    DiagnosticsData: 5,
};

type ResponseDataHandler = (buffer: Buffer) => void;
type ResponseErrorHandler = (err: Error) => void;

interface ResponseHandler {
    data: ResponseDataHandler;
    error: ResponseErrorHandler;
}

export class IpcClient {
    private waitingResponses: Record<number, ResponseHandler[]> = {};
    private responseTimeouts: Record<number, NodeJS.Timeout> = {};
    private socket: dgram.Socket;

    constructor(private socketAddress: string) {
        this.socket = this.createSocket();
    }

    connectAsync(): Promise<void> {
        return new Promise<void>(res => {
            this.socket.connect(SocketPort, this.socketAddress, res);
        });
    }

    closeAsync(): Promise<void> {
        return new Promise<void>(res => {
            this.socket.close(res);
        });
    }

    /* #region Messages/Requests */

    async setPidParamsAsync(pidId: number, p: number, i: number, d: number): Promise<void> {
        // 4 bytes: uint32 messageId
        // 4 bytes: uint32 pidId
        // 8 bytes: double p
        // 8 bytes: double i
        // 8 bytes: double d
        // total: 32 bytes

        const request = Buffer.allocUnsafe(32);
        let offset = 0;

        offset = request.writeUInt32LE(MessageIds.SetPidParam, offset);
        offset = writePidParams({ pidId, p, i, d }, request, offset);

        await this.sendMessageAsync(request);
    }

    async getPidParamsAsync(pidId: number): Promise<PidParams> {
        // 4 bytes: uint32 messageId
        // 4 bytes: uint32 pidId
        // total: 8 bytes

        const request = Buffer.allocUnsafe(8);
        let offset = 0;

        offset = request.writeUInt32LE(MessageIds.GetPidParam, offset);
        offset = request.writeInt32LE(pidId, offset);

        const response = await this.sendMessageAndWaitAsync(request, MessageIds.PidParamData);

        if (response.length < 32)
            throw new Error(`Incomplete response for "GetPid". Wanted: 32 bytes. Got: ${response.length} bytes.`);

        const responseParams = readPidParams(response, 4); // offset 4 bytes for message ID

        if (responseParams.pidId != pidId)
            throw new Error(`Got response for wrong PID. Wanted: ${pidId}. Got: ${responseParams.pidId}.`);

        return responseParams;
    }

    async getDiagnosticsAsync(): Promise<Diagnostics> {
        // 4 bytes: uint32 messageId
        // total: 4 bytes

        const request = Buffer.allocUnsafe(4);

        request.writeUInt32LE(MessageIds.GetDiagnostics, 0);

        const response = await this.sendMessageAndWaitAsync(request, MessageIds.DiagnosticsData);
        
        return readDiagnostics(response, 4); // offset 4 bytes for message ID
    }

    /* #endregion Messages/Requests */

    /* #region Private Handler Code */

    private createSocket(): dgram.Socket {
        const socket = dgram.createSocket("udp4");

        socket.on("message", this.onSocketMessage.bind(this));
        socket.on("error", this.onSocketError.bind(this));

        return socket;
    }

    private onSocketMessage(message: Buffer, _: dgram.RemoteInfo) {
        if (message.length < 4) return; // Didn't even contain a message ID

        const messageId = message.readUInt32LE(0);
        const waiting = this.waitingResponses[ messageId ];

        if (waiting && waiting.length > 0) {
            waiting.splice(0, waiting.length).forEach(handler => handler.data(message));
        }

        const timeout = this.responseTimeouts[ messageId ];

        if (timeout) {
            clearTimeout(timeout);
        }

        delete this.responseTimeouts[ messageId ];
    }

    private onSocketError(error: Error): void {
        console.error("Got socket error:");
        console.error(error);

        this.socket.close(async () => {
            this.socket = this.createSocket();

            console.error("Reconnecting...");

            await this.connectAsync();

            console.error("Reconnected.");
        });
    }

    private async sendMessageAndWaitAsync(request: Buffer, responseTypeId: number): Promise<Buffer> {
        // Make sure to set up the response promise *before* sending the message in the EXTREMELY slim
        // chance that the response would arrive before "sendMessageAsync" resolves
        const responsePromise = this.waitForResponse(responseTypeId);

        await this.sendMessageAsync(request);

        return await responsePromise;
    }

    private sendMessageAsync(data: Buffer): Promise<void> {
        return new Promise<void>((res, rej) => {
            this.socket.send(data, (error, _) => {
                if (error) return rej(error);

                return res();
            });
        });
    }

    private waitForResponse(messageId: number): Promise<Buffer> {
        return new Promise<Buffer>((res, rej) => {
            const responseHandler = { data: res, error: rej };

            if (!this.waitingResponses[ messageId ]) {
                this.waitingResponses[ messageId ] = [];
            }

            this.waitingResponses[ messageId ].push(responseHandler);
            this.responseTimeouts[ messageId ] = setTimeout(this.timeoutResponse.bind(this, messageId), RequestTimeout);
        });
    }

    private timeoutResponse(messageId: number) {
        const waiting = this.waitingResponses[ messageId ];
        const error = new Error(`IPC request timed out (no response from remote device in ${RequestTimeout}ms)`);

        if (waiting && waiting.length > 0) {
            waiting.splice(0, waiting.length).forEach(handler => handler.error(error));
        }

        delete this.responseTimeouts[ messageId ];
    }

    /* #endregion Private Handler Code */
}