export interface Angles {
    pitch: number;
    roll: number;
    heading: number;
}

export interface PidParams {
    pidId: number;
    p: number;
    i: number;
    d: number;
}

export interface Diagnostics {
    orientation: Angles;
    targetOrientation: Angles;
    pitchPid: PidParams;
    rollPid: PidParams;
    yawPid: PidParams;
    pitchControl: number;
    rollControl: number;
    yawControl: number;
}