import chalk from "chalk";
import express from "express";
import handlebars from "express-handlebars";
import net from "net";
import path from "path";
import process from "process";

import { promises as fs } from "fs";
import { PidIds } from "@/constants";
import { IpcClient } from "@/ipc";
import { ConfigFile } from "@/models";
import { promisify } from "@/utils";

export async function main() {
    const { argv } = process;

    if (argv.length < 3) {
        console.warn(`Usage: ${argv[ 1 ]} <drone address>`);
        throw new Error("Drone address is required.");
    }

    const droneAddress = argv[ 2 ];

    // Load config file
    const configFilePath = path.join(process.cwd(), "config.json");

    try {
        const configStat = await fs.stat(configFilePath);

        if (!configStat.isFile()) {
            throw new Error("Fatal error: config.json must be a file");
        }
    } catch (err) {
        throw new Error(`Fatal error: config.json file not found in ${configFilePath}`);
    }

    const configJson = await fs.readFile(configFilePath, { encoding: "utf8" });
    const config: ConfigFile = JSON.parse(configJson);

    // Set up IPC client
    const ipcClient = new IpcClient(droneAddress);

    await ipcClient.connectAsync();

    // Set up web server
    const app = express();
    const hbs = handlebars.create();

    // Variables for use in views
    app.locals.PidIds = PidIds;

    app.set("views", path.join(__dirname, "../views"));
    app.set("view engine", "handlebars");
    app.engine("handlebars", hbs.engine);
    app.use(express.static("public"));
    app.use(express.urlencoded({ extended: true }));

    // Index endpoint
    app.get("/", async (_, res) => {
        const pitchPid = await ipcClient.getPidParamsAsync(PidIds.Pitch);
        const rollPid = await ipcClient.getPidParamsAsync(PidIds.Roll);
        const yawPid = await ipcClient.getPidParamsAsync(PidIds.Yaw);

        res.render("index", { pitchPid, rollPid, yawPid });
    });

    app.post("/setpids", async (req, res) => {
        const kP_pitch = +req.body[ "kP_pitch" ];
        const kI_pitch = +req.body[ "kI_pitch" ];
        const kD_pitch = +req.body[ "kD_pitch" ];
        const kP_roll = +req.body[ "kP_roll" ];
        const kI_roll = +req.body[ "kI_roll" ];
        const kD_roll = +req.body[ "kD_roll" ];
        const kP_yaw = +req.body[ "kP_yaw" ];
        const kI_yaw = +req.body[ "kI_yaw" ];
        const kD_yaw = +req.body[ "kD_yaw" ];
        const allValues = [ kP_pitch, kI_pitch, kD_pitch, kP_roll, kI_roll, kD_roll, kP_yaw, kI_yaw, kD_yaw ]

        if (allValues.some(v => isNaN(v))) {
            return res.status(400).send("Invalid request format");
        }

        await ipcClient.setPidParamsAsync(PidIds.Pitch, kP_pitch, kI_pitch, kD_pitch);
        await ipcClient.setPidParamsAsync(PidIds.Roll, kP_roll, kI_roll, kD_roll);
        await ipcClient.setPidParamsAsync(PidIds.Yaw, kP_yaw, kI_yaw, kD_yaw);

        res.redirect("/");
    });

    app.get("/diagnostics", async (_, res) => { 
        const diagnostics = await ipcClient.getDiagnosticsAsync();
        
        res.json(diagnostics);
    });

    app.use((error: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (res.headersSent) {
            console.error("Headers sent");
            return next(error);
        }

        if (error) {
            console.error(`Error in request for path ${req.path}:`);
            console.error(error);

            return res.render("error", { error });
        }

        return next();
    });

    // Start web server
    const serverConnections: net.Socket[] = [];
    const server = app.listen(config.httpPort, "0.0.0.0", () => {
        console.log(`Web server is listening on ${chalk.green(`http://localhost:${config.httpPort}/`)}`);
    });

    server.on("connection", socket => {
        serverConnections.push(socket);

        socket.on("close", () => {
            const socketIndex = serverConnections.indexOf(socket);

            if (socketIndex >= 0) {
                serverConnections.splice(socketIndex, 1);
            }
        });
    });

    process.on("SIGINT", async () => {
        console.log("Shutting down web server...");

        try {
            // First close the server so it doesn't accept new connections
            const closePromise = promisify(callback => server.close(callback));

            // Now close all existing connections
            serverConnections.splice(0, serverConnections.length).forEach(socket => socket.destroy());

            // Wait for the server to shut down
            await closePromise;
        } catch (err) {
            console.error("Couldn't gracefully shut down web server");
            console.error(err);
        }

        console.log("Shutting down IPC channel...");

        try {
            await ipcClient.closeAsync();
        } catch (err) {
            console.error("Couldn't gracefully shut down IPC channel");
            console.error(err);
        }

        console.log("Exiting.");
        process.exit(0);
    });
}