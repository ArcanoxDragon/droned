import path from "path";

import * as tsconfigPaths from "tsconfig-paths";

const tsconfig = require(path.join(__dirname, "../tsconfig.json"));

tsconfigPaths.register({
    baseUrl: __dirname,
    paths: tsconfig.compilerOptions.paths,
});

import { main } from "app";

main().catch((err: unknown) => {
    console.error("Error in application:");
    console.error(err);
    process.exit(1);
});