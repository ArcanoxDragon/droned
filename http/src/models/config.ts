export interface ConfigFile {
    httpPort: number;
}

export const DefaultConfig: ConfigFile = {
    httpPort: 8888,
};