type VoidPromiseCallback = (error: Error | undefined) => void;
type ResultPromiseCallback<TResult> = (error: Error | undefined, result: TResult) => void;
type PromiseCallback<TResult> = TResult extends void ? VoidPromiseCallback : ResultPromiseCallback<TResult>;

type PromiseHandler<TResult> = (callback: PromiseCallback<TResult>) => void;

export function promisify<TResult = void>(handler: PromiseHandler<TResult>): Promise<TResult> {
    return new Promise<TResult>((res, rej) => {
        const callback = (error: Error | undefined, result?: TResult): void => error ? rej(error) : res(result as TResult);

        handler(callback as PromiseCallback<TResult>);
    });
}