declare module "unix-dgram" {
    import { Socket as DgramSocket } from "dgram";

    namespace unix {
        export type SocketType = "unix_dgram";
        
        export interface Socket extends Omit<DgramSocket, "connect"> {
            connect(address: string, callback?: () => void): void;
        }
        
        export function createSocket(type: SocketType): Socket;
    }
    
    export = unix;
}