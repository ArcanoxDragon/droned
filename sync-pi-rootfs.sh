#!/bin/bash

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TMP_DIR=$ROOT_DIR/tmp
DST_DIR=/opt/cross-pi-gcc/sysroot

mkdir -p $TMP_DIR
rsync -vR --progress -rulz --delete-after --safe-links "$@" dronepi:/{lib,usr,etc/ld.so.conf.d,opt/vc/lib} $TMP_DIR
sudo mkdir -p $DST_DIR
sudo cp -r $TMP_DIR/* $DST_DIR
rm -rf $TMP_DIR
