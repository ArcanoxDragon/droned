#pragma once

#include <netinet/in.h>
#include <vector>
#include <SDL2/SDL_thread.h>

#include "../types.h"

#define SOCKET_PORT             9888
#define SOCKET_BUFFER_SIZE      1024

/* #region IPC Data Structures */

enum ipc_messagetype_t : uint32_t {
    nop,
    set_pid_param,
    get_pid_param,
    pid_param_data,
    get_diagnostics,
    diagnostics_data,
};

/* #region Shared */

datastruct ipc_pid_params_t {
    int32_t pidId;
    double p, i, d;
};

/* #endregion Shared */

/* #region Responses */

template <typename data_t>
datastruct ipc_response_t {
    ipc_messagetype_t type;
    data_t data;
};

/**
 * Data layout:
 * 
 * 8 bytes: double orientation.pitch @ 0
 * 8 bytes: double orientation.roll @ 8
 * 8 bytes: double orientation.heading @ 16
 * 8 bytes: double targetOrientation.pitch @ 24
 * 8 bytes: double targetOrientation.roll @ 32
 * 8 bytes: double targetOrientation.heading @ 40
 * 4 bytes: uint32 pitchPid.id @ 48
 * 8 bytes: double pitchPid.p @ 52
 * 8 bytes: double pitchPid.i @ 60
 * 8 bytes: double pitchPid.d @ 68
 * 4 bytes: uint32 rollPid.id @ 76
 * 8 bytes: double rollPid.p @ 80
 * 8 bytes: double rollPid.i @ 88
 * 8 bytes: double rollPid.d @ 96
 * 4 bytes: uint32 yawPid.id @ 104
 * 8 bytes: double yawPid.p @ 108
 * 8 bytes: double yawPid.i @ 116
 * 8 bytes: double yawPid.d @ 124
 * 8 bytes: double pitchControl @ 132
 * 8 bytes: double rollControl @ 140
 * 8 bytes: double yawControl @ 148
 * 
 * Total size: 156 bytes
 */
datastruct ipc_diagnostics_data_t {
    angles_t orientation, targetOrientation;
    ipc_pid_params_t pitchPid, rollPid, yawPid;
    double pitchControl, rollControl, yawControl;
};

/* #endregion Responses */

/* #region Requests */

struct ipc_clientdata_t {
    sockaddr clientAddr;
    socklen_t addrLen;
};

union ipc_requestdata_t {
    ipc_pid_params_t pid;
};

datastruct ipc_request_t {
    ipc_messagetype_t type;
    ipc_requestdata_t data;
};

/* #endregion Requests */

/* #endregion IPC Data Structures */

class UdpSocket {
public:
    static void initialize();
    static void dispose();

    static bool listening();

    template <typename data_t>
    static void sendMessage(const ipc_clientdata_t *to, const ipc_response_t<data_t> &message) {
        uint8_t buffer[sizeof(message)];

        memcpy(buffer, &message, sizeof(message));
        sendto(socketFd, buffer, sizeof(message), MSG_CONFIRM, &to->clientAddr, to->addrLen);
    }

private:
    static int socketFd;
    static volatile bool reading;
    static SDL_Thread *socketThreadId;

    static int socketThread(void *data);
    static void handleMessage(const ipc_clientdata_t *from, const ipc_request_t *message);
};