#include <netinet/in.h>
#include <stdexcept>
#include <stdio.h>
#include <sstream>
#include <sys/poll.h>
#include <sys/socket.h>
#include <unistd.h>
#include "SDL2/SDL_events.h"
#include "SDL2/SDL_thread.h"

#include "../ErrorMessage.h"
#include "../events/user_events.h"
#include "UdpSocket.h"

/* #region Public */

void UdpSocket::initialize() {
    socketFd = socket(AF_INET, SOCK_DGRAM, 0);

    if (socketFd < 0) {
        throw "Error creating socket";
    }

    sockaddr_in serverAddr;

    memset(&serverAddr, 0, sizeof(sockaddr_in));

    // Populate the address struct with family/address/port
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(SOCKET_PORT);

    // Bind to the socket
    if (bind(socketFd, (sockaddr *) &serverAddr, sizeof(sockaddr_in))) {
        throw std::runtime_error(ErrorMessage() << "Error: could not bind to port " << SOCKET_PORT);
    }

    socketThreadId = SDL_CreateThread(UdpSocket::socketThread, "Socket_Read", NULL);
}

void UdpSocket::dispose() {
    reading = false;

    close(socketFd);
    SDL_WaitThread(socketThreadId, NULL);

    socketThreadId = NULL;
}

bool UdpSocket::listening() {
    return reading;
}

/* #endregion Public */

/* #region Private */

int UdpSocket::socketFd = -1;
volatile bool UdpSocket::reading = false;
SDL_Thread *UdpSocket::socketThreadId = NULL;

int UdpSocket::socketThread(void *data) {
    reading = true;

    pollfd pfd;
    int numAvailable, numRead;
    char buffer[SOCKET_BUFFER_SIZE];
    sockaddr clientAddr;
    socklen_t addrLen = sizeof(clientAddr);
    ipc_request_t message;

    pfd.fd = socketFd;
    pfd.events = POLLIN;

    while (reading) {
        // Try to wait for a read with a 1-second timeout
        numAvailable = poll(&pfd, 1, 1000);

        if (numAvailable < 0) {
            fprintf(stderr, "Error in socket thread: could not read from socket\n");
            reading = false;
            break;
        } else if (pfd.revents & POLLIN) {
            numRead = recvfrom(socketFd, buffer, SOCKET_BUFFER_SIZE, MSG_WAITALL, &clientAddr, &addrLen);

            if (numRead < 0) {
                fprintf(stderr, "Error in socket thread: could not read from socket\n");
                reading = false;
                break;
            }

            if (numRead >= sizeof(ipc_messagetype_t)) {
                memset(&message, 0, sizeof(message));
                memcpy(&message, buffer, numRead);

                // This will go into an event data property and get freed by the global event handler.
                ipc_clientdata_t *from = new ipc_clientdata_t();

                from->clientAddr = clientAddr;
                from->addrLen = addrLen;

                handleMessage(from, &message);
            }
        }
    }

    close(socketFd);

    return 0;
}

void UdpSocket::handleMessage(const ipc_clientdata_t *from, const ipc_request_t *message) {
    // Allocate memory for the event data object. This will be freed by the global event handler.
    ipc_requestdata_t *data = new ipc_requestdata_t();
    SDL_Event event;

    // Zero out the event
    SDL_zero(event);

    // Copy the data from the IPC message to the one we will put in the event
    memcpy(data, &message->data, sizeof(*data));

    // Populate the user event data
    event.type = getUserEventTypeId(evIpcMessage);
    event.user.code = (int32_t) message->type;
    event.user.data1 = data;
    event.user.data2 = (void *) from;

    // Push the event to the SDL event queue
    SDL_PushEvent(&event);
}

/* #endregion Private */