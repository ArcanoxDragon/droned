#include <math.h>
#include <stdio.h>

#include "../mathutil.h"
#include "PidController.h"

/* #region Public */

PidController::PidController(double pGain, double iGain, double dGain) :
    kP(pGain),
    kI(iGain),
    kD(dGain) { }

PidController::~PidController() {
    delete this->derivativeAverage;
}

double PidController::get(double target, double actual, double dt) {
    // Calculate early values
    this->error = (target - actual) * this->scale;

    /* #region Proportional */

    if (feq(this->proportionalExponent, 1.0)) {
        this->pOutput = this->kP * clamp(this->error, this->minError, this->maxError);
    } else {
        this->pOutput = this->kP * copysign(pow(fabs(this->error), this->proportionalExponent), this->error);
    }

    /* #endregion Proportional */

    /* #region Derivative */

    this->delta = (actual - this->lastValue) * this->scale / dt;
    this->dOutput = this->kD * this->delta;

    // Apply derivative moving average if necessary
    if (this->derivativeAverage) {
        this->dOutput = this->derivativeAverage->get(this->dOutput);
    }

    /* #endregion Derivative  */

    /* #region Integral */

    // Apply I-lock and calculate integral
    this->iAutoLocked = this->iAutoLock && fabs(this->delta) > this->iLockThreshold;
    this->isILocked = this->iLocked || this->iAutoLocked;

    if (this->iAutoLocked && this->iResetsOnLock) {
        this->integral = 0.0;
    }

    if (!this->isILocked) {
        this->integral = clamp(this->integral + this->error * this->kI * dt, this->minIntegral, this->maxIntegral);
    }

    this->iOutput = this->integral;

    /* #endregion Integral */

    this->output = clamp(this->pOutput + this->iOutput - this->dOutput, this->minOutput, this->maxOutput);
    this->lastValue = actual;
    this->lastTarget = target;
    this->lastError = this->error;

    return this->output;
}

double PidController::peek() {
    return this->output;
}

void PidController::reset(bool soft) {
    if (soft) {
        this->integral -= this->integral * this->kI;
    } else {
        this->integral = 0;
    }
}

void PidController::setDerivativeAverageAlpha(double alpha) {
    if (alpha < 0.0) {
        delete this->derivativeAverage;
        this->derivativeAverage = NULL;
        return;
    }

    if (this->derivativeAverage) {
        this->derivativeAverage->setAlpha(alpha);
    } else {
        this->derivativeAverage = new ExponentialMovingAverage(alpha);
    }
}

double PidController::getDerivativeAverageAlpha() {
    if (!this->derivativeAverage) {
        return -1.0;
    }

    return this->derivativeAverage->getAlpha();
}

double PidController::getP() { return this->pOutput; }
double PidController::getI() { return this->iOutput; }
double PidController::getD() { return this->dOutput; }

/* #endregion Public */