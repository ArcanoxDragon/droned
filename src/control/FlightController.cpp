#include <algorithm>
#include <stddef.h>
#include <stdio.h>

#include "../devices/accelerometer/Accelerometer.h"
#include "../devices/motors/Motors.h"
#include "../events/EventBus.h"
#include "../events/user_events.h"
#include "../ipc/UdpSocket.h"
#include "../devices/joystick/Joystick.h"
#include "../devices/joystick/JoyMappings_SwitchProController.h"
#include "../terminal.h"
#include "../timeutils.h"
#include "FlightController.h"

#define MOTOR_RIGHT_FRONT   1
#define MOTOR_RIGHT_REAR    0
#define MOTOR_LEFT_REAR     2
#define MOTOR_LEFT_FRONT    3

#define MAX_TARGET_ANGLE            15.0    // degrees
#define MAX_TARGET_YAWRATE          45.0    // degrees/sec
#define MAX_THROTTLE_ADJUSTMENT     0.25
#define THROTTLE_TRIM_RATE          0.25    // throttle/sec
#define YAW_RATE_GAIN               0.001   // throttle-per-deg/sec

#define PID_ID_PITCH                0
#define PID_ID_ROLL                 1
#define PID_ID_YAW                  2

#define PID_YAW_SCALE               (1.0 / 360.0)
#define PID_TILT_SCALE              (1.0 / 360.0)
#define PID_TILT_EXPONENT           1.0
#define THROTTLE_PID_MAX_OUTPUT     0.25
#define THROTTLE_PID_MAX_ERROR      1.0
#define THROTTLE_PID_DAVG_ALPHA     0.25

// TODO: Store PID values in settings file somewhere
#define PID_PITCH_P                 2.0
#define PID_PITCH_I                 0.0
#define PID_PITCH_D                 0.025

#define PID_ROLL_P                  2.0
#define PID_ROLL_I                  0.0
#define PID_ROLL_D                  0.25

#define PID_YAW_P                   0.0
#define PID_YAW_I                   0.0
#define PID_YAW_D                   0.0

#define SENSOR_UPDATE_FREQUENCY     800     // Hz
#define SENSOR_UPDATE_INTERVAL      (1.0 / SENSOR_UPDATE_FREQUENCY)

double lastSensorUpdate = 0.0;

FlightController::FlightController() :
    pitchPid(PID_PITCH_P, PID_PITCH_I, PID_PITCH_D),
    rollPid(PID_ROLL_P, PID_ROLL_I, PID_ROLL_D),
    yawPid(PID_YAW_P, PID_YAW_I, PID_YAW_D) {

    EventBus::get()->listen(this);

    pitchPid.minError = -THROTTLE_PID_MAX_ERROR;
    pitchPid.maxError = THROTTLE_PID_MAX_ERROR;
    pitchPid.minOutput = -THROTTLE_PID_MAX_OUTPUT;
    pitchPid.maxOutput = THROTTLE_PID_MAX_OUTPUT;
    pitchPid.scale = PID_TILT_SCALE;
    pitchPid.proportionalExponent = PID_TILT_EXPONENT;
    pitchPid.setDerivativeAverageAlpha(THROTTLE_PID_DAVG_ALPHA);
    rollPid.minError = -THROTTLE_PID_MAX_ERROR;
    rollPid.maxError = THROTTLE_PID_MAX_ERROR;
    rollPid.minOutput = -THROTTLE_PID_MAX_OUTPUT;
    rollPid.maxOutput = THROTTLE_PID_MAX_OUTPUT;
    rollPid.scale = PID_TILT_SCALE;
    rollPid.proportionalExponent = PID_TILT_EXPONENT;
    rollPid.setDerivativeAverageAlpha(THROTTLE_PID_DAVG_ALPHA);
    yawPid.minOutput = -THROTTLE_PID_MAX_OUTPUT;
    yawPid.maxOutput = THROTTLE_PID_MAX_OUTPUT;
    yawPid.scale = PID_YAW_SCALE;
    yawPid.setDerivativeAverageAlpha(THROTTLE_PID_DAVG_ALPHA);
    lastTime = curTime = getTime();
}

FlightController::~FlightController() { }

/* #region Public */

void FlightController::update() {
    curTime = getTime();
    dt = curTime - lastTime;

    if (dt < 0) {
        // Something weird happened (clock looped around or something); reset lastTime and bail
        lastTime = curTime;
        return;
    }
    
    if (dt < 2e-4) {
        // Don't run the loop if dt was less than 0.2 ms
        return;
    }

    lastTime = curTime;

    // Update our target angles
    targetPitch = Joystick::getY2() * MAX_TARGET_ANGLE;
    targetRoll = Joystick::getX2() * MAX_TARGET_ANGLE;
    throttleAdjustment = -Joystick::getY1() * MAX_THROTTLE_ADJUSTMENT;
    handleButtonInput(dt);

    if (buttonResetHeading) {
        targetHeading = orientation.heading;
    } else if (buttonYawLeft) {
        targetHeading -= MAX_TARGET_YAWRATE * dt;
    } else if (buttonYawRight) {
        targetHeading += MAX_TARGET_YAWRATE * dt;
    }

    if (targetHeading < 0.0) targetHeading += 360.0;
    if (targetHeading >= 360.0) targetHeading -= 360.0;
    
    if (orientation.heading - targetHeading > 180.0) {
        targetHeading += 360.0;
    } else if (targetHeading - orientation.heading > 180.0) {
        targetHeading -= 360.0;
    }

    if (throttleTrim < 0.01) {
        pitchPid.reset();
        rollPid.reset();
        yawPid.reset();
    }
    
    double sensorUpdateInterval = curTime - lastSensorUpdate;

    if (sensorUpdateInterval >= SENSOR_UPDATE_INTERVAL) {
        lastSensorUpdate = curTime;

        // Update our sensor inputs
        Accelerometer::update();
        Accelerometer::getOrientation(orientation);
        Accelerometer::getAngularVelocity(angularVelocity);
        Accelerometer::getAcceleration(acceleration);

        // Update our control values
        pitchControl = pitchPid.get(targetPitch, orientation.pitch, sensorUpdateInterval);
        rollControl = rollPid.get(targetRoll, orientation.roll, sensorUpdateInterval);
        yawControl = yawPid.get(targetHeading, orientation.heading, sensorUpdateInterval);
    }

    // Decide on motor power
    baseThrottle = throttleTrim + throttleAdjustment;
    throttleLeftFront = baseThrottle + pitchControl + rollControl - yawControl;
    throttleRightFront = baseThrottle + pitchControl - rollControl + yawControl;
    throttleLeftRear = baseThrottle - pitchControl + rollControl + yawControl;
    throttleRightRear = baseThrottle - pitchControl - rollControl - yawControl;

    // Set motor throttles
    double throttles[NUM_MOTORS];

    if (throttleTrim > 0.01) {
        throttles[MOTOR_LEFT_FRONT] = throttleLeftFront;
        throttles[MOTOR_RIGHT_FRONT] = throttleRightFront;
        throttles[MOTOR_LEFT_REAR] = throttleLeftRear;
        throttles[MOTOR_RIGHT_REAR] = throttleRightRear;
    } else {
        throttles[MOTOR_LEFT_FRONT] = 0.0;
        throttles[MOTOR_RIGHT_FRONT] = 0.0;
        throttles[MOTOR_LEFT_REAR] = 0.0;
        throttles[MOTOR_RIGHT_REAR] = 0.0;
    }

    Motors::setThrottles(throttles[0], throttles[1], throttles[2], throttles[3]);
    
    // Calculate how long it took to run the loop
    loopTime = getTime() - curTime;
}

void FlightController::printDebug() {
    printf("Orientation:\n");
    printf("P: %6.2f   R: %6.2f   H: %6.2f\n\n", orientation.pitch, orientation.roll, orientation.heading);

    printf("Angular Velocity:\n");
    printf("P: %6.2f   R: %6.2f   H: %6.2f\n\n", angularVelocity.pitch, angularVelocity.roll, angularVelocity.heading);

    printf("Acceleration:\n");
    printf("X: %6.2f   Y: %6.2f   Z: %6.2f\n\n", acceleration.x, acceleration.y, acceleration.z);

    printf("DT:   %6.4f ms    LT:    %6.4f ms  Init: %s\n", dt * 1000.0, loopTime * 1000.0, initialized ? "YES" : "NO");
    printf("ThTr: %6.2f%%    BaseTh: %6.2f%%\n", throttleTrim * 100.0, baseThrottle * 100.0);
    printf("TgtP: %6.2f deg   TgtR: %6.2f deg  TgtH: %6.2f deg\n\n", targetPitch, targetRoll, targetHeading);
    printf("PITCH                    ROLL                    YAW\n\n");
    printf("P: %6.3f  K: %6.3f     P: %6.3f  K: %6.3f    P: %6.3f  K: %6.3f\n", pitchPid.getP(), pitchPid.kP, rollPid.getP(), rollPid.kP, yawPid.getP(), yawPid.kP);
    printf("I: %6.3f  K: %6.3f     I: %6.3f  K: %6.3f    I: %6.3f  K: %6.3f\n", pitchPid.getI(), pitchPid.kI, rollPid.getI(), rollPid.kI, yawPid.getI(), yawPid.kI);
    printf("D: %6.3f  K: %6.3f     D: %6.3f  K: %6.3f    D: %6.3f  K: %6.3f\n", pitchPid.getD(), pitchPid.kD, rollPid.getD(), rollPid.kD, yawPid.getD(), yawPid.kD);
    printf("O: %6.3f                O: %6.3f               O: %6.3f\n\n", pitchControl, rollControl, yawControl);
    printf("MOTORS\n\n");
    printf("LF: %5.2f    RF: %5.2f\n", throttleLeftFront, throttleRightFront);
    printf("LR: %5.2f    RR: %5.2f\n", throttleLeftRear, throttleRightRear);
}

void FlightController::handleEvent(const SDL_Event *event) {
    if (event->type == getUserEventTypeId(evIpcMessage)) {
        ipc_requestdata_t *request = (ipc_requestdata_t *) (event->user.data1);
        ipc_clientdata_t *clientData = (ipc_clientdata_t *) (event->user.data2);

        switch (event->user.code) {
            case (int) ipc_messagetype_t::set_pid_param: {
                // Got UDP request to set parameters for a specific PID controller

                this->handleSetPidEvent(request);
                break;
            }
            case (int) ipc_messagetype_t::get_pid_param: {
                // Got UDP request to send back current parameters for a specific PID controller

                this->handleGetPidEvent(request, clientData);
                break;
            }
            case (int) ipc_messagetype_t::get_diagnostics: {
                // Got UDP request to send back diagnostic data

                this->handleGetDiagnosticsEvent(request, clientData);
                break;
            }
        }
    }
}

/* #endregion Public */

/* #region Private */

void FlightController::handleButtonInput(double dt) {
    buttonKillSwitch = Joystick::getButton(JOY_BUTTON_ZL);
    buttonResetHeading = Joystick::getButton(JOY_BUTTON_ZR);
    buttonThrottleTrimUp = Joystick::getButton(JOY_BUTTON_X);
    buttonThrottleTrimDown = Joystick::getButton(JOY_BUTTON_A);
    buttonYawLeft = Joystick::getButton(JOY_BUTTON_L);
    buttonYawRight = Joystick::getButton(JOY_BUTTON_R);

    if (!buttonKillSwitch || !Joystick::isConnected()) {
        throttleTrim = 0.0;
    } else {
        if (buttonThrottleTrimUp) {
            throttleTrim = std::min(1.0, throttleTrim + THROTTLE_TRIM_RATE * dt);
        } else if (buttonThrottleTrimDown) {
            throttleTrim = std::max(0.0, throttleTrim - THROTTLE_TRIM_RATE * dt);
        }
    }
}

void FlightController::handleSetPidEvent(const ipc_requestdata_t *request) {
    switch (request->pid.pidId) {
        case PID_ID_PITCH: {
            pitchPid.kP = request->pid.p;
            pitchPid.kI = request->pid.i;
            pitchPid.kD = request->pid.d;
            break;
        }
        case PID_ID_ROLL: {
            rollPid.kP = request->pid.p;
            rollPid.kI = request->pid.i;
            rollPid.kD = request->pid.d;
            break;
        }
        case PID_ID_YAW: {
            yawPid.kP = request->pid.p;
            yawPid.kI = request->pid.i;
            yawPid.kD = request->pid.d;
            break;
        }
    }
}

void FlightController::handleGetPidEvent(const ipc_requestdata_t *request, const ipc_clientdata_t *clientData) {
    ipc_response_t<ipc_pid_params_t> response;

    response.type = ipc_messagetype_t::pid_param_data;
    response.data.pidId = request->pid.pidId;

    switch (request->pid.pidId) {
        case PID_ID_PITCH: {
            response.data.p = pitchPid.kP;
            response.data.i = pitchPid.kI;
            response.data.d = pitchPid.kD;
            break;
        }
        case PID_ID_ROLL: {
            response.data.p = rollPid.kP;
            response.data.i = rollPid.kI;
            response.data.d = rollPid.kD;
            break;
        }
        case PID_ID_YAW: {
            response.data.p = yawPid.kP;
            response.data.i = yawPid.kI;
            response.data.d = yawPid.kD;
            break;
        }
        default: {
            // Indicate this was an invalid PID data request
            response.data.pidId = -1;
            break;
        }
    }

    UdpSocket::sendMessage(clientData, response);
}

void FlightController::handleGetDiagnosticsEvent(const ipc_requestdata_t *request, const ipc_clientdata_t *clientData) {
    ipc_response_t<ipc_diagnostics_data_t> response;

    response.type = ipc_messagetype_t::diagnostics_data;

    // Current orientation
    response.data.orientation = this->orientation;

    // Target orientation
    response.data.targetOrientation.pitch = this->targetPitch;
    response.data.targetOrientation.roll = this->targetRoll;
    response.data.targetOrientation.heading = this->targetHeading;

    // Pitch PID
    response.data.pitchPid.pidId = PID_ID_PITCH;
    response.data.pitchPid.p = this->pitchPid.getP();
    response.data.pitchPid.i = this->pitchPid.getI();
    response.data.pitchPid.d = this->pitchPid.getD();

    // Roll PID
    response.data.rollPid.pidId = PID_ID_ROLL;
    response.data.rollPid.p = this->rollPid.getP();
    response.data.rollPid.i = this->rollPid.getI();
    response.data.rollPid.d = this->rollPid.getD();

    // Yaw PID
    response.data.yawPid.pidId = PID_ID_YAW;
    response.data.yawPid.p = this->yawPid.getP();
    response.data.yawPid.i = this->yawPid.getI();
    response.data.yawPid.d = this->yawPid.getD();

    // Control values
    response.data.pitchControl = this->pitchPid.peek();
    response.data.rollControl = this->rollPid.peek();
    response.data.yawControl = this->yawPid.peek();

    UdpSocket::sendMessage(clientData, response);
}

/* #endregion Private */