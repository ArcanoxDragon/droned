#pragma once

#include <limits>

#include "../filters/ExponentialMovingAverage.h"

class PidController {
public:
    double kP, kI, kD;
    double minError = -std::numeric_limits<double>::max();
    double maxError = std::numeric_limits<double>::max();
    double minOutput = -1.0;
    double maxOutput = 1.0;
    double minIntegral = -1.0;
    double maxIntegral = 1.0;
    double proportionalExponent = 1.0;
    double scale = 1.0;
    double iLockThreshold = 0.0;
    bool iLocked = false;
    bool iAutoLock = false;
    bool iResetsOnLock = false;

    PidController(double pGain = 0.0, double iGain = 0.0, double dGain = 0.0);
    ~PidController();

    double get(double target, double actual, double dt);
    double peek();
    void reset(bool soft = false);

    void setDerivativeAverageAlpha(double alpha);
    double getDerivativeAverageAlpha();

    double getP();
    double getI();
    double getD();

private:
    ExponentialMovingAverage *derivativeAverage = NULL;
    bool iAutoLocked = false;
    bool isILocked = false;
    double error = 0.0;
    double delta = 0.0;
    double lastValue = 0.0;
    double lastTarget = 0.0;
    double lastError = 0.0;
    double integral = 0.0;
    double pOutput = 0.0;
    double iOutput = 0.0;
    double dOutput = 0.0;
    double output = 0.0;
};