#pragma once

#include "../events/EventBus.h"
#include "PidController.h"

class FlightController : public EventHandler {
public:
    FlightController();
    ~FlightController();

    void update();
    void printDebug();

    void handleEvent(const SDL_Event *event);

private:
    PidController pitchPid;
    PidController rollPid;
    PidController yawPid;

    bool initialized = false;
    double lastTime = 0.0;
    double curTime = 0.0;
    double dt = 0.0;
    double loopTime = 0.0;

    angles_t orientation;
    angles_t angularVelocity;
    vector_t acceleration;

    double targetPitch = 0.0;
    double targetRoll = 0.0;
    double targetHeading = 0.0;
    double throttleAdjustment = 0.0;
    double baseThrottle = 0.0;

    double pitchControl;
    double rollControl;
    double yawControl;

    double throttleTrim = 0.0;
    double throttleLeftFront = 0.0;
    double throttleRightFront = 0.0;
    double throttleLeftRear = 0.0;
    double throttleRightRear = 0.0;

    bool buttonKillSwitch = false;
    bool buttonResetHeading = false;
    bool buttonThrottleTrimUp = false;
    bool buttonThrottleTrimDown = false;
    bool buttonYawLeft = false;
    bool buttonYawRight = false;

    void handleButtonInput(double dt);
    
    void handleSetPidEvent(const ipc_requestdata_t *request);
    void handleGetPidEvent(const ipc_requestdata_t *request, const ipc_clientdata_t *clientData);
    void handleGetDiagnosticsEvent(const ipc_requestdata_t *request, const ipc_clientdata_t *clientData);
};