#pragma once

#include <algorithm>
#include <limits>

#define clamp(val, lowerBound, upperBound) std::min(std::max(val, lowerBound), upperBound)

#define feq(a, b) (fabs(a - b) < std::numeric_limits<__typeof(a)>::epsilon())

double map(double val, double srcMin, double srcMax, double dstMin, double dstMax, bool clampOutput = true);