#include <SDL2/SDL.h>

#include "../config.h"
#include "../terminal.h"
#include "../devices/accelerometer/Accelerometer.h"
#include "../devices/accelerometer/ImuAccelerometerDriver.h"
#include "ImuTestProgram.h"

/* #region Public */

ImuTestProgram::ImuTestProgram() {
    EventBus::get()->listen(this);
}

ImuTestProgram::~ImuTestProgram() {
    EventBus::get()->unlisten(this);
}

int ImuTestProgram::setup() {
    printf("Starting IMU test program...\n");

    // Explicitly configure the Accelerometer system to use the IMU driver
    Accelerometer::initialize(new ImuAccelerometerDriver(I2C_ADDR_IMU));

    // Make stdout explicitly-buffered
    setvbuf(stdout, NULL, _IOFBF, 0);

    // Clear the screen and move the cursor to top-left
    clearTerm();
    cursorPos(1, 1);
    fflush(stdout);

    return 0;
}

bool ImuTestProgram::loop() {
    uint32_t time = SDL_GetTicks();

    if (time >= nextPrint) {
        printInfo();
        nextPrint = time + PRINT_INTERVAL;
    }

    return true;
}

void ImuTestProgram::handleEvent(const SDL_Event *event) {

}

/* #endregion Public */

/* #region Private */

void ImuTestProgram::printInfo() {
    cursorPos(1, 1);

    angles_t orientation;
    angles_t angularVelocity;
    vector_t accel;

    Accelerometer::update();
    Accelerometer::getOrientation(orientation);
    Accelerometer::getAngularVelocity(angularVelocity);
    Accelerometer::getAcceleration(accel);

    printf("Orientation:\n");
    printf("P: %6.2f   R: %6.2f   H: %6.2f\n\n", orientation.pitch, orientation.roll, orientation.heading);

    printf("Angular Velocity:\n");
    printf("P: %6.2f   R: %6.2f   H: %6.2f\n\n", angularVelocity.pitch, angularVelocity.roll, angularVelocity.heading);

    printf("Acceleration:\n");
    printf("X: %6.2f   Y: %6.2f   Z: %6.2f\n\n", accel.x, accel.y, accel.z);

    fflush(stdout);
}

/* #endregion Private */