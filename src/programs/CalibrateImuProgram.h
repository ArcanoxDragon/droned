#pragma once

#include <stddef.h>

#include "../devices/accelerometer/ImuAccelerometerDriver.h"
#include "Program.h"

class CalibrateImuProgram : public Program {
public:
    CalibrateImuProgram();

    int setup();
    bool loop();

private:
    enum state_t {
        state_init,
        state_start,
        state_calib_gyro,
        state_calib_acc,
        state_calib_mag,
        state_test,
        state_finished,
    };

    ImuAccelerometerDriver *driver = NULL;
    state_t state = state_init;
    state_t lastState = state_init;
    bool gyroCalibrated = false;
    bool accCalibrated = false;
    bool magCalibrated = false;

};