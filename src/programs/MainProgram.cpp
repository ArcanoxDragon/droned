#include <wiringPi.h>
#include <wiringPiI2C.h>

#include "../config.h"
#include "../devices/accelerometer/Accelerometer.h"
#include "../devices/accelerometer/ImuAccelerometerDriver.h"
#include "../devices/motors/Motors.h"
#include "../events/user_events.h"
#include "../ipc/UdpSocket.h"
#include "../devices/joystick/Joystick.h"
#include "../devices/joystick/JoystickDriver.h"
#include "../devices/joystick/JoyMappings_SwitchProController.h"
#include "../mathutil.h"
#include "../terminal.h"
#include "MainProgram.h"

/* #region Public */

MainProgram::MainProgram() {
    this->controller = new FlightController();

    EventBus::get()->listen(this);
}

MainProgram::~MainProgram() {
    delete this->controller;

    EventBus::get()->unlisten(this);
}

int MainProgram::setup() {
    // Make stdout explicitly-buffered
    setvbuf(stdout, NULL, _IOFBF, 0);

    // Clear the screen and move the cursor to top-left
    clearTerm();
    cursorPos(1, 1);
    fflush(stdout);

    return 0;
}

bool MainProgram::loop() {
    uint32_t time = SDL_GetTicks();

    this->controller->update();

    if (time >= nextPrint) {
        printInfo();
        nextPrint = time + PRINT_INTERVAL;
    }

    return true;
}

void MainProgram::handleEvent(const SDL_Event *event) { }

/* #endregion Public */

/* #region Private */

void MainProgram::printInfo() {
    cursorPos(1, 1);

    double jx1 = Joystick::getX1();
    double jy1 = Joystick::getY1();
    double jx2 = Joystick::getX2();
    double jy2 = Joystick::getY2();
    bool buttonKillSwitch = Joystick::getButton(JOY_BUTTON_ZL);
    bool buttonSetHeading = Joystick::getButton(JOY_BUTTON_ZR);
    bool buttonThrottleTrimUp = Joystick::getButton(JOY_BUTTON_X);
    bool buttonThrottleTrimDown = Joystick::getButton(JOY_BUTTON_A);
    bool buttonYawLeft = Joystick::getButton(JOY_BUTTON_L);
    bool buttonYawRight = Joystick::getButton(JOY_BUTTON_R);

    printf("Joystick:\n");
    printf("X1: %4.2f\tY1: %4.2f\nX2: %4.2f\tY2: %4.2f\n\n", jx1, jy1, jx2, jy2);
    printf(
        "KSW:  %d  SH:  %d  THR_UP:  %d   THR_DN:  %d  YL:  %d  YR:  %d\n\n",
        buttonKillSwitch,
        buttonSetHeading,
        buttonThrottleTrimUp,
        buttonThrottleTrimDown,
        buttonYawLeft,
        buttonYawRight);

    ImuAccelerometerDriver *driver = ImuAccelerometerDriver::get();
    bno055_calib_stat_t calibStat;

    driver->getCalibrationStatus(calibStat);

    printf("              SYS MAG GYR ACC\n");
    printf("Calibration:  %02x  %02x  %02x  %02x\n\n", calibStat.sys, calibStat.mag, calibStat.gyro, calibStat.acc);

    controller->printDebug();

    fflush(stdout);
}

/* #endregion Private */