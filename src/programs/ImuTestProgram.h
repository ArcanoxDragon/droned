#pragma once

#include "../events/EventBus.h"
#include "Program.h"

class ImuTestProgram : public Program, public EventHandler {
public:
    ImuTestProgram();
    ~ImuTestProgram();

    int setup();
    bool loop();
    void handleEvent(const SDL_Event *event);

private:
    uint32_t nextPrint = 0;

    void printInfo();
};