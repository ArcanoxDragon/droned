#include <SDL2/SDL.h>

#include "../devices/motors/Motors.h"
#include "KillProgram.h"

/* #region Public */

KillProgram::KillProgram() { }

int KillProgram::setup() {
    Motors::setThrottles(0.0, 0.0, 0.0, 0.0);
    
    printf("All motors stopped.\n");

    return 0;
}

bool KillProgram::loop() { return false; }

/* #endregion Public */