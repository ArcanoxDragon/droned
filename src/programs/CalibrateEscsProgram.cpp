#include <math.h>
#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <SDL2/SDL.h>

#include "../config.h"
#include "../devices/motors/Motors.h"
#include "../devices/joystick/Joystick.h"
#include "../mathutil.h"
#include "../terminal.h"
#include "../timeutils.h"
#include "../utils.h"
#include "CalibrateEscsProgram.h"

#define CALIBRATED_MIN_THROTTLE     0.02
#define THROTTLE_DECREASE_RATE      1.0     // 1.0 throttle units per second

/* #region Public */

CalibrateEscsProgram::CalibrateEscsProgram(int motorIndex, bool testOnly) : motorIndex(motorIndex), testOnly(testOnly) { }

CalibrateEscsProgram::CalibrateEscsProgram(int motorIndex) : CalibrateEscsProgram(motorIndex, false) { }

int CalibrateEscsProgram::setup() {
    printf("Starting ESC calibration process...\n");

    disableEcho();

    Motors::setThrottles(0.0, 0.0, 0.0, 0.0);

    if (this->testOnly) {
        state = state_test;
    } else {
        state = state_ensure_disconnected;
    }

    this->lastTime = getTime();
    return 0;
}

bool CalibrateEscsProgram::loop() {
    bool justEnteredState = state != lastState;
    state_t nextState = state;
    double curTime = getTime();
    double dt = curTime - lastTime;

    switch (state) {
        case state_ensure_disconnected: {
            if (justEnteredState) {
                printf("Make sure ESC is disconnected from power and does not have a prop installed!!!\n");
                printf("Press ENTER when ready.\n");
            }

            throttle = 0.0;

            if (testForEnterKey()) {
                nextState = state_full_throttle;
            }

            break;
        }
        case state_full_throttle: {
            if (justEnteredState) {
                printf("Throttle is at 100%%. Connect power to the ESC now.\n");
                printf("Wait for the ESC to boot up, and press ENTER after it sounds a beep acknowledging the \"max throttle\" value.\n");
            }

            throttle = 1.0;

            if (testForEnterKey()) {
                nextState = state_ramp_throttle_down;
            }

            break;
        }
        case state_ramp_throttle_down: {
            if (justEnteredState) {
                printf("Throttle will now slowly move to 0%%...\n");
            }

            double newThrottle = std::max(CALIBRATED_MIN_THROTTLE, throttle - (THROTTLE_DECREASE_RATE * dt));

            if (throttle > CALIBRATED_MIN_THROTTLE + 1e-5) {
                clearLine(true);

                if (newThrottle <= CALIBRATED_MIN_THROTTLE + 1e-5) {
                    printf("Press ENTER when the ESC has acknowledged the calibration.\n");
                } else {
                    printf("Throttle: %5.1f%%", throttle * 100.0);
                }

                fflush(stdout);
            }

            throttle = newThrottle;

            if (throttle <= CALIBRATED_MIN_THROTTLE + 1e-5 && testForEnterKey()) {
                nextState = state_test;
            }

            break;
        }
        case state_test: {
            if (justEnteredState) {
                printf("Now use the joystick to test the motor response. Press ENTER when finished.\n");
            }

            double jy2 = -Joystick::getY2();

            throttle = map(jy2, 0.0, 1.0, 0.0, MOTOR_MAX_THROTTLE);

            clearLine(true);
            printf("Throttle: %5.1f%%", throttle * 100.0);
            fflush(stdout);

            if (testForEnterKey()) {
                enableEcho();
                clearTerm();
                printf("\n");
                fflush(stdout);
                nextState = state_finished;
            }

            break;
        }
    }

    if (this->motorIndex >= 0) {
        Motors::setThrottleDangerous(this->motorIndex, throttle);
    } else {
        Motors::setThrottlesDangerous(throttle, throttle, throttle, throttle);
    }

    delay(10);

    lastState = state;
    state = nextState;
    lastTime = curTime;

    return state != state_finished;
}

/* #endregion Public */