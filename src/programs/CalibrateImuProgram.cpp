#include <math.h>
#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <SDL2/SDL.h>

#include "../config.h"
#include "../devices/accelerometer/Accelerometer.h"
#include "../devices/joystick/Joystick.h"
#include "../mathutil.h"
#include "../terminal.h"
#include "../utils.h"
#include "CalibrateImuProgram.h"

/* #region Public */

CalibrateImuProgram::CalibrateImuProgram() { }

int CalibrateImuProgram::setup() {
    printf("Starting IMU calibration process...\n");
    disableEcho();

    // Construct a new IMU driver so that we have a direct handle to it
    driver = new ImuAccelerometerDriver(I2C_ADDR_IMU);

    // Install our own driver
    Accelerometer::initialize(driver);

    state = state_start;
    return 0;
}

bool CalibrateImuProgram::loop() {
    bool justEnteredState = state != lastState;
    state_t nextState = state;
    bno055_calib_stat_t calibStatus;

    driver->getCalibrationStatus(calibStatus);

    switch (state) {
        case state_start: {
            if (justEnteredState) {
                printf("Make sure the drone is right-side up on a flat, level, and stable surface.\n");
                printf("Press ENTER when ready.\n");
            }

            if (testForEnterKey()) {
                // Reset the device so that calibration starts anew
                driver->resetDevice();

                // Put the device in NDOF mode
                driver->setOperationMode(bno055_opermode_ndof);

                nextState = state_calib_gyro;
            }

            break;
        }
        case state_calib_gyro: {
            if (justEnteredState) {
                printf("Keep the drone perfectly still until the Gyroscope calibration level reaches \"3\".\n");
            }

            if (calibStatus.gyro >= 3 || gyroCalibrated) {
                if (!gyroCalibrated) {
                    clearLine(true);
                    printf("Gyroscope calibration level: %d\n", calibStatus.gyro);
                    printf("Gyroscope calibrated! Press ENTER to move to the next step.\n");
                    gyroCalibrated = true;
                }

                if (testForEnterKey()) {
                    nextState = state_calib_acc;
                }
            } else {
                clearLine(true);
                printf("Gyroscope calibration level: %d", calibStatus.gyro);
                fflush(stdout);
                delay(10);
            }

            break;
        }
        case state_calib_acc: {
            if (justEnteredState) {
                printf("Hold the drone still for several seconds, and then rotate it VERY gently in a 45-degree\n");
                printf("increment along one axis. Hold it still again for several seconds, and then rotate it again\n");
                printf("in the same direction. Hold it still again, and repeat this process for each axis\n");
                printf("or until the Accelerometer calibration level reaches \"3\".\n");
            }

            if (calibStatus.acc >= 3 || accCalibrated) {
                if (!accCalibrated) {
                    clearLine(true);
                    printf("Accelerometer calibration level: %d\n", calibStatus.acc);
                    printf("Accelerometer calibrated! Press ENTER to move to the next step.\n");
                    accCalibrated = true;
                }

                if (testForEnterKey()) {
                    nextState = state_calib_mag;
                }
            } else {
                clearLine(true);
                printf("Accelerometer calibration level: %d", calibStatus.acc);
                fflush(stdout);
                delay(10);
            }

            break;
        }
        case state_calib_mag: {
            if (justEnteredState) {
                printf("Move the drone slowly and randomly through the air so that it experiences a variety of orientations.\n");
                printf("Try moving it in figure-8 motions at different angles.\n");
                printf("Repeat this process until the Magnetometer calibration level reaches \"3\".\n");
                printf("Press ENTER to skip magnetometer calibration if it is not needed.\n");
            }

            if (calibStatus.mag >= 3 || magCalibrated) {
                if (!magCalibrated) {
                    clearLine(true);
                    printf("Magnetometer calibration level: %d\n", calibStatus.mag);
                    printf("Magnetometer calibrated! Press ENTER to move to the next step.\n");
                    magCalibrated = true;
                }
            } else {
                clearLine(true);
                printf("Magnetometer calibration level: %d", calibStatus.mag);
                fflush(stdout);
                delay(10);
            }

            if (testForEnterKey()) {
                nextState = state_test;
            }

            break;
        }
        case state_test: {
            if (justEnteredState) {
                // Save the calibration data
                bno055_calibration_t calibration;

                driver->getCalibrationData(calibration);

                if (!ImuCalibration::saveCalibration(calibration)) {
                    fprintf(stderr, "Error! Could not save calibration data to the filesystem.\n");
                    return false;
                }

                // Make stdout explicitly-buffered
                setvbuf(stdout, NULL, _IOFBF, 0);
            }

            angles_t orientation;
            vector_t accel;

            Accelerometer::update();
            Accelerometer::getOrientation(orientation);
            Accelerometer::getAcceleration(accel);

            clearTerm();
            cursorPos(1, 1);
            printf("The calibration data has been saved.\n");
            printf("Test out the calibration by moving the drone around and observing that\n");
            printf("the values correlate with the drone's actual orientation and movement.\n");
            printf("Press ENTER when finished.\n\n");

            printf("Pitch:    %6.2f deg\n", orientation.pitch);
            printf("Roll:     %6.2f deg\n", orientation.roll);
            printf("Heading:  %6.2f deg\n\n", orientation.heading);

            printf("X:        %6.2f m/s^2\n", accel.x);
            printf("Y:        %6.2f m/s^2\n", accel.y);
            printf("Z:        %6.2f m/s^2", accel.z);

            fflush(stdout);
            delay(10);

            if (testForEnterKey()) {
                // Make stdout line-buffered (default)
                setvbuf(stdout, NULL, _IOLBF, 0);
                nextState = state_finished;
            }

            break;
        }
    }

    lastState = state;
    state = nextState;

    return state != state_finished;
}

/* #endregion Public */