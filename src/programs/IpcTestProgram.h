#pragma once

#include "../events/EventBus.h"
#include "Program.h"

class IpcTestProgram : public Program, public EventHandler {
public:
    IpcTestProgram();
    ~IpcTestProgram();

    int setup();
    bool loop();
    void handleEvent(const SDL_Event *event);
};