#include <math.h>
#include <stdexcept>
#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <SDL2/SDL.h>

#include "../config.h"
#include "../devices/accelerometer/Accelerometer.h"
#include "../devices/joystick/Joystick.h"
#include "../mathutil.h"
#include "../terminal.h"
#include "../timeutils.h"
#include "../utils.h"
#include "CalibrateOffsetProgram.h"

/* #region Public */

CalibrateOffsetProgram::CalibrateOffsetProgram() { }

int CalibrateOffsetProgram::setup() {
    printf("Starting orientation offset calibration process...\n");
    disableEcho();
    clearTerm();
    cursorPos(1, 1);
    fflush(stdout);

    // Delete the existing calibration and reset the internal calibration
    OffsetCalibration::deleteCalibration();
    Accelerometer::reloadOffsetCalibration();

    state = state_start;
    return 0;
}

bool CalibrateOffsetProgram::loop() {
    bool justEnteredState = state != lastState;
    state_t nextState = state;
    double curTime = getTime();
    angles_t orientation;

    Accelerometer::update();
    Accelerometer::getOrientation(orientation);

    switch (state) {
        case state_start: {
            if (justEnteredState) {
                printf("Make sure the drone is right-side up on a flat, level, and stable surface.\n");
                printf("Press ENTER when ready.\n");
            }

            if (testForEnterKey()) {
                nextState = state_calib;
            }

            break;
        }
        case state_calib: {
            if (justEnteredState) {
                printf("Keep the drone perfectly still for 10 seconds.\n");
                startedTime = curTime;
                startPitch = orientation.pitch;
                startRoll = orientation.roll;
            }

            double secondsLeft = 10.0 - (curTime - startedTime);

            clearLine(true);
            printf("Time left: %d", (int) ceil(secondsLeft));
            fflush(stdout);
            delay(10);

            if (fabs(orientation.pitch - startPitch) > 0.25 || fabs(orientation.roll - startRoll) > 0.25) {
                nextState = state_keepstill;
            }

            if (secondsLeft <= 0.0) {
                nextState = state_test;
            }

            break;
        }
        case state_keepstill: {
            if (justEnteredState) {
                clearTerm();
                cursorPos(1, 1);
                printf("The drone moved too much! Please keep the drone still.\n");
                fflush(stdout);
                startedTime = curTime;
            }

            if (curTime - startedTime >= 2.0) {
                nextState = state_calib;
            }

            break;
        }
        case state_test: {
            if (justEnteredState) {
                // Save the calibration data
                offset_calibration_t calibration;

                calibration.pitchOffset = orientation.pitch;
                calibration.rollOffset = orientation.roll;

                try {
                    OffsetCalibration::saveCalibration(calibration);
                } catch (std::runtime_error &err) {
                    fprintf(stderr, "Error! Could not save calibration data to the filesystem: %s\n", err.what());
                    return false;
                }

                // Reload our new calibration data
                Accelerometer::reloadOffsetCalibration();

                // Make stdout explicitly-buffered
                setvbuf(stdout, NULL, _IOFBF, 0);
            }

            clearTerm();
            cursorPos(1, 1);
            printf("The calibration data has been saved.\n");
            printf("Test out the calibration by tilting the drone in different directions and ensuring that\n");
            printf("it reads 0.0 degrees pitch/roll when it is returned to its flat, upright position.\n");
            printf("Press ENTER when finished.\n\n");

            printf("Pitch:    %6.2f deg\n", orientation.pitch);
            printf("Roll:     %6.2f deg\n", orientation.roll);

            fflush(stdout);
            delay(10);

            if (testForEnterKey()) {
                // Make stdout line-buffered (default)
                setvbuf(stdout, NULL, _IOLBF, 0);
                nextState = state_finished;
            }

            break;
        }
    }

    lastState = state;
    state = nextState;

    return state != state_finished;
}

/* #endregion Public */