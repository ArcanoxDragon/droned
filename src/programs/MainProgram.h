#pragma once

#include "../control/FlightController.h"
#include "../events/EventBus.h"
#include "Program.h"

class MainProgram : public Program, public EventHandler {
public:
    MainProgram();
    ~MainProgram();

    int setup();
    bool loop();
    void handleEvent(const SDL_Event *event);

private:
    FlightController *controller = NULL;
    uint32_t nextPrint = 0;

    void printInfo();
};