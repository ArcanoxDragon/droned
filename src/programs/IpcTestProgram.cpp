#include <SDL2/SDL.h>

#include "../events/user_events.h"
#include "../ipc/UdpSocket.h"
#include "IpcTestProgram.h"

/* #region Public */

IpcTestProgram::IpcTestProgram() {
    EventBus::get()->listen(this);
}

IpcTestProgram::~IpcTestProgram() {
    EventBus::get()->unlisten(this);
}

int IpcTestProgram::setup() {
    printf("Starting IPC test program. UDP socket is listening on port %d.\n", SOCKET_PORT);
    
    return 0;
}

bool IpcTestProgram::loop() { return UdpSocket::listening(); }

void IpcTestProgram::handleEvent(const SDL_Event *event) {
    if (event->type == getUserEventTypeId(userevent_t::evIpcMessage)) {
        ipc_requestdata_t *message = (ipc_requestdata_t *) (event->user.data1);
        ipc_clientdata_t *clientData = (ipc_clientdata_t *) (event->user.data2);

        switch (event->user.code) {
            case (int) ipc_messagetype_t::set_pid_param: {
                ipc_pid_params_t pid = message->pid;

                printf("Set PID event: set PID %d to P=%f, I=%f, D=%f\n", pid.pidId, pid.p, pid.i, pid.d);

                break;
            }
            case (int) ipc_messagetype_t::get_pid_param: {
                ipc_pid_params_t pid = message->pid;

                printf("Get PID event: get params for PID %d\n", pid.pidId);

                ipc_response_t<ipc_pid_params_t> response;

                response.type = ipc_messagetype_t::pid_param_data;
                response.data.pidId = pid.pidId;
                response.data.p = 1.5;
                response.data.i = 2.5;
                response.data.d = 3.5;

                UdpSocket::sendMessage(clientData, response);

                break;
            }
        }
    }
}

/* #endregion Public */