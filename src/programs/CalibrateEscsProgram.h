#pragma once

#include "Program.h"

class CalibrateEscsProgram : public Program {
public:
    CalibrateEscsProgram(int motorIndex, bool testOnly);
    CalibrateEscsProgram(int motorIndex);

    int setup();
    bool loop();

private:
    enum state_t {
        state_init,
        state_ensure_disconnected,
        state_full_throttle,
        state_ramp_throttle_down,
        state_test,
        state_finished,
    };

    bool testOnly = false;
    int motorIndex = -1;
    state_t state = state_init;
    state_t lastState = state_init;
    double throttle = 0.0;
    double lastTime = 0.0;
};