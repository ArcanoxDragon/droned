#pragma once

#include <stdint.h>
#include <SDL2/SDL.h>

class Program {
public:
    virtual ~Program() { };

    virtual uint32_t getInitFlags() { return 0; }
    virtual int setup() = 0;
    virtual bool loop() = 0;

protected:
    Program() { }
};