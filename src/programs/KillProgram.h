#pragma once

#include "Program.h"

class KillProgram : public Program {
public:
    KillProgram();

    int setup();
    bool loop();
};