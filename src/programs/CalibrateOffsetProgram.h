#pragma once

#include <stddef.h>

#include "../devices/accelerometer/Accelerometer.h"
#include "../devices/accelerometer/OffsetCalibration.h"
#include "Program.h"

class CalibrateOffsetProgram : public Program {
public:
    CalibrateOffsetProgram();

    int setup();
    bool loop();

private:
    enum state_t {
        state_init,
        state_start,
        state_calib,
        state_keepstill,
        state_test,
        state_finished,
    };

    state_t state = state_init;
    state_t lastState = state_init;
    double startedTime = 0.0;
    double startPitch = 0.0;
    double startRoll = 0.0;

};