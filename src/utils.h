#pragma once

#include <stdint.h>
#include <type_traits>

#define GUID_LENGTH     16

/**
 *  Checks whether two GUIDs are equal. Each array MUST have a length of 16.
 */
bool guidEquals(uint8_t const first[], uint8_t const second[]);

/**
 * Waits for the enter key to be pressed
 */
void waitForEnterKey();

/**
 * Drains all pending characters from stdin until either a \n character is found
 * or there are no more characters remaining. Returns whether or not a \n character
 * was found.
 */
bool testForEnterKey();

/**
 * Returns \c value with the bits represented by \c mask replaced by the same bits from \c newBits
 */
template <typename T>
T setBits(T value, T mask, T newBits) {
    static_assert(std::is_same<T, bool>()
        || std::is_same<T, uint8_t>()
        || std::is_same<T, uint16_t>()
        || std::is_same<T, uint32_t>()
        || std::is_same<T, uint64_t>()
        || std::is_same<T, int8_t>()
        || std::is_same<T, int16_t>()
        || std::is_same<T, int32_t>()
        || std::is_same<T, int64_t>(),
        "Invalid data type for setBits");
        
    T newValue = value & ~mask;
    
    return newValue | (newBits & mask);
}