#pragma once

#include <array>
#include <cassert>
#include <initializer_list>
#include <stddef.h>
#include <stdexcept>
#include <type_traits>

template <typename Tvalue, size_t Size>
class Vector {
public:
    Vector() = default;
    Vector(const Vector<Tvalue, Size> &) = default;

    Vector(std::initializer_list<Tvalue> values) {
        assert(values.size() == Size);

        // Copy values into our data array
        std::copy(values.begin(), values.end(), this->values);
    }

    const Tvalue at(int idx) const {
        if (idx < 0 || idx >= Size) {
            throw std::out_of_range("Vector index out-of-range");
        }

        return this->values[idx];
    }

    Tvalue &operator[](int idx) {
        if (idx < 0 || idx >= Size) {
            throw std::out_of_range("Vector index out-of-range");
        }

        return this->values[idx];
    }

    template<typename U>
    friend auto operator+(const Vector &lhs, const Vector<U, Size> &rhs) {
        Vector<decltype(lhs.at(0) + rhs.at(0)), Size> result;

        for (int i = 0; i < Size; i++) {
            result[i] = lhs.at(i) + rhs.at(i);
        }

        return result;
    }

    template<typename U>
    inline friend auto operator-(const Vector &lhs, const Vector<U, Size> &rhs) {
        return lhs + (-rhs);
    }

    inline friend Vector operator-(const Vector &rhs) {
        return rhs * -1;
    }

    template<typename U>
    friend auto operator*(const Vector &lhs, const U &rhs) {
        Vector<decltype(lhs.at(0) *rhs), Size> result;

        for (int i = 0; i < Size; i++) {
            result[i] = lhs.at(i) * rhs;
        }

        return result;
    }

    template<typename U>
    inline friend auto operator*(const U &lhs, const Vector &rhs) {
        return rhs * lhs;
    }

private:
    Tvalue values[Size];
};

// Sized subclasses with nice accessors
template<typename Tvalue>
class Vector2 : public Vector<Tvalue, 2> {
public:
    Vector2(const Vector<Tvalue, 2> &other) : Vector<Tvalue, 2>(other) { }
    Vector2(std::initializer_list<Tvalue> values) : Vector<Tvalue, 2>(values) { }

    inline Tvalue x() const { return this->at(0); }
    inline Tvalue y() const { return this->at(1); }

    inline Tvalue x(const Tvalue &value) { (*this)(0) = value; }
    inline Tvalue y(const Tvalue &value) { (*this)(1) = value; }
};

template<typename Tvalue>
class Vector3 : public Vector<Tvalue, 3> {
public:
    Vector3(const Vector<Tvalue, 3> &other) : Vector<Tvalue, 3>(other) { }
    Vector3(std::initializer_list<Tvalue> values) : Vector<Tvalue, 3>(values) { }

    inline Tvalue x() const { return this->at(0); }
    inline Tvalue y() const { return this->at(1); }
    inline Tvalue z() const { return this->at(2); }

    inline Tvalue x(const Tvalue &value) { (*this)(0) = value; }
    inline Tvalue y(const Tvalue &value) { (*this)(1) = value; }
    inline Tvalue z(const Tvalue &value) { (*this)(2) = value; }
};

// Typed aliases
typedef Vector2<float> Vector2f;
typedef Vector2<double> Vector2d;
typedef Vector2<int> Vector2i;
typedef Vector3<float> Vector3f;
typedef Vector3<double> Vector3d;
typedef Vector3<int> Vector3i;