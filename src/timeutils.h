#pragma once

#include <time.h>

inline double getTime() {
    timespec curtimespec;

    clock_gettime(CLOCK_MONOTONIC_RAW, &curtimespec);

    return ((double) curtimespec.tv_sec) + ((double) curtimespec.tv_nsec) * 1e-9;
}