#pragma once

#include <vector>
#include "SDL2/SDL.h"

class EventHandler {
public:
    virtual void handleEvent(const SDL_Event *event) = 0;
};

class EventBus {
public:
    static void initialize();
    static void dispose();
    static EventBus *get();

    EventBus();

    void post(const SDL_Event *event) const;
    void listen(EventHandler *eventHandler);
    void unlisten(EventHandler *eventHandler);
private:
    static EventBus *instance;

    std::vector<EventHandler *> eventHandlers;
};