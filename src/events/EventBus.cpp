#include "../output.h"
#include "EventBus.h"

/* #region Public */

void EventBus::initialize() {
    LOG_VERBOSE("Initializing EventBus...\n");
    
    instance = new EventBus();
}

void EventBus::dispose() {
    delete instance;
}

EventBus *EventBus::get() {
    return instance;
}

EventBus::EventBus() : eventHandlers() { }

void EventBus::post(const SDL_Event *event) const {
    for (EventHandler *eventHandler : eventHandlers) {
        eventHandler->handleEvent(event);
    }
}

void EventBus::listen(EventHandler *eventHandler) {
    for (EventHandler *handler : eventHandlers) {
        if (handler == eventHandler)
            // Don't add a listener twice
            return;
    }

    eventHandlers.push_back(eventHandler);
}

void EventBus::unlisten(EventHandler *eventHandler) {
    bool didRemove;

    for (auto it = eventHandlers.begin(); it != eventHandlers.end();) {
        if (*it == eventHandler) {
            eventHandlers.erase(it);
            didRemove = true;
            break;
        } else {
            ++it;
        }
    }

    if (didRemove) {
        // "unlisten" is unlikely to be called very often at all. It's okay to
        // spend a little extra time cleaning up unused memory when this happens.
        eventHandlers.shrink_to_fit();
    }
}

/* #endregion Public */

/* #region Private */

EventBus *EventBus::instance = NULL;

/* #endregion Private */