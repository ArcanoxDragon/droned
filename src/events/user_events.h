#pragma once

enum userevent_t {
    evIpcMessage,
    
    evLast,
};

void registerUserEvents();
int getUserEventTypeId(userevent_t eventType);
bool isUserEvent(uint32_t eventTypeId);