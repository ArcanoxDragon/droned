#include <map>
#include <SDL2/SDL_events.h>

#include "user_events.h"

std::map<userevent_t, uint32_t> eventTypeIds;

void registerUserEvents() {
    for (int event = 0; event != userevent_t::evLast; event++) {
        userevent_t eventType = static_cast<userevent_t>(event);

        eventTypeIds[eventType] = SDL_RegisterEvents(1);
    }
}

int getUserEventTypeId(userevent_t eventType) {
    if (!eventTypeIds.count(eventType))
        return -1;

    return eventTypeIds[eventType];
}

bool isUserEvent(uint32_t eventTypeId) {
    for (auto it = eventTypeIds.begin(); it != eventTypeIds.end(); it++) {
        if ((*it).second == eventTypeId) {
            return true;
        }
    }
    
    return false;
}