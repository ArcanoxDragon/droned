#include <poll.h>
#include <stdio.h>
#include <stdarg.h>
#include <termios.h>

#include "terminal.h"

const char *csiStr = "\x1b[";
// const char *csiStr = "[";

void printEscapeCode(const char *fmt, ...) {
    va_list args1, args2;
    va_start(args1, fmt);
    va_copy(args2, args1);

    // Figure out length needed for temp buffer
    int len = vsnprintf(NULL, 0, fmt, args1);
    va_end(args1);

    // Use the input format/args to write the temp buffer
    char buf[len + sizeof(char)];
    vsprintf(buf, fmt, args2);
    va_end(args2);

    // Print the temp buffer prefixed with the CSI
    printf("%s%s", csiStr, buf);
}

void clearTerm() {
    printEscapeCode("2J"); // Clear whole screen
}

void clearLine(bool resetCursor /*  = false */) {
    printEscapeCode("2K"); // Clear whole line

    if (resetCursor) {
        printEscapeCode("1G"); // Move cursor absolute (column 1)
    }
}

void cursorPos(int x, int y) {
    printEscapeCode("%i;%iH", x, y);
}

bool isEchoEnabled() {
    termios flags;

    if (tcgetattr(fileno(stdin), &flags) != 0)
        return false;
        
    return flags.c_lflag & ECHO != 0;
}

bool disableEcho() {
    termios flags;

    if (tcgetattr(fileno(stdin), &flags) != 0)
        return false;

    flags.c_lflag &= ~ECHO;

    if (tcsetattr(fileno(stdin), TCSAFLUSH, &flags) != 0)
        return false;

    return true;
}

bool enableEcho() {
    termios flags;

    if (tcgetattr(fileno(stdin), &flags) != 0)
        return false;

    flags.c_lflag |= ECHO;

    if (tcsetattr(fileno(stdin), TCSAFLUSH, &flags) != 0)
        return false;

    return true;
}

bool charAvailable() {
    pollfd fds;
    
    fds.fd = fileno(stdin);
    fds.events = POLLIN;
    
    int result = poll(&fds, 1, 0);
    
    return result > 0;
}