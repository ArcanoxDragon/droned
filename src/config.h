#pragma once

#include <chrono>

using namespace std::chrono_literals;

#define LOOP_INTERVAL       10us /* 100 microseconds */
#define PRINT_INTERVAL      20

// I2C Addresses
#define I2C_ADDR_ACCELEROMETER  0x19
#define I2C_ADDR_IMU            0x28
#define I2C_ADDR_PCA9685        0x40

// PWM parameters
#define PWM_THROTTLE_MIN    90
#define PWM_THROTTLE_MAX    240