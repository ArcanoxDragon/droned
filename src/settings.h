#pragma once

#include "json/json.h"

#define SETTINGS_PATH   "settings.json"

class Settings {
public:
    static void load();
    static json get();
    
private:
    static bool loaded;
    static json instance;
    
    static json createDefault();
};