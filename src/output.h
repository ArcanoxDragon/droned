#pragma once

#include <stdio.h>

#ifdef VERBOSE
    #define LOG_VERBOSE(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
    #define LOG_VERBOSE(fmt, ...)
#endif