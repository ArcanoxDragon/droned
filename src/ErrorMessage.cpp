#include "ErrorMessage.h"

/* #region Public */

std::string ErrorMessage::str() const {
    return this->stream.str();
}

ErrorMessage::operator std::string() const {
    return this->stream.str();
}

/* #endregion Public */