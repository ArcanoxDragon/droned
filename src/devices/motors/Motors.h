#pragma once

#include <stdint.h>

#define NUM_MOTORS          4
#define MOTOR_MAX_THROTTLE  0.55

class MotorDriver;

class Motors {
public:
    static void initialize();
    static void initialize(MotorDriver *driver);
    static void dispose();

    static void setThrottles(double th0, double th1, double th2, double th3);
    static void setThrottlesDangerous(double th0, double th1, double th2, double th3);
    static void setThrottle(uint8_t motor, double throttle);
    static void setThrottleDangerous(uint8_t motor, double throttle);

private:
    static MotorDriver *driver;
};