#pragma once

#include "Motors.h"
#include "MotorDriver.h"

struct __attribute__((packed)) pca9685_i2c_1ch_t {
    uint8_t reg;
    uint16_t delaySteps, offSteps;
};

struct __attribute__((packed)) pca9685_i2c_4ch_t {
    uint8_t reg;
    uint16_t delayStepsCh0, offStepsCh0;
    uint16_t delayStepsCh1, offStepsCh1;
    uint16_t delayStepsCh2, offStepsCh2;
    uint16_t delayStepsCh3, offStepsCh3;
};

class Pca9685MotorDriver : public MotorDriver {
public:
    Pca9685MotorDriver(uint8_t i2cAddress);
    ~Pca9685MotorDriver();

    void initialize();
    void setThrottles(double th0, double th1, double th2, double th3);
    void setThrottle(uint8_t motor, double throttle);

private:
    uint8_t i2cAddress = 0;
    int fdDriver = -1;
    uint16_t delayStepsCache[NUM_MOTORS];
    uint16_t offStepsCache[NUM_MOTORS];

    void calculateSteps(double throttle, uint16_t *delaySteps, uint16_t *offSteps);

    bool isEnabled();
    void enableDriver(bool enable);
    void setFrequency(double frequency);
};