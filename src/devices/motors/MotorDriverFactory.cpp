#include <stdexcept>

#include "../../config.h"
#include "../../ErrorMessage.h"
#include "MotorDriverFactory.h"
#include "Pca9685MotorDriver.h"

using stringstream = std::stringstream;

/* #region Public */

MotorDriver *MotorDriverFactory::createDriver(const string name) {
    if (name == "pca9685") {
        return new Pca9685MotorDriver(I2C_ADDR_PCA9685);
    }

    throw std::runtime_error(ErrorMessage() << "Error: no motor driver named \"" << name << "\"");
}

/* #endregion Public */