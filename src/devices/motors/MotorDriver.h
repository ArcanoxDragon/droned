#pragma once

#include <stdint.h>

class MotorDriver {
public:
    virtual ~MotorDriver() { }

    virtual void initialize() = 0;
    virtual void setThrottles(double th0, double th1, double th2, double th3) = 0;
    virtual void setThrottle(uint8_t motor, double throttle) = 0;

protected:
    MotorDriver() { }
};