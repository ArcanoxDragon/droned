#include <stddef.h>
#include <string>

#include "../../mathutil.h"
#include "../../output.h"
#include "../../settings.h"
#include "Motors.h"
#include "MotorDriver.h"
#include "MotorDriverFactory.h"

using string = std::string;

/* #region Public */

void Motors::initialize() {
    json settings = Settings::get();
    string driverName = settings["motorDriver"].get<string>();
    
    initialize(MotorDriverFactory::createDriver(driverName));
}

void Motors::initialize(MotorDriver *driver) {
    LOG_VERBOSE("Initializing Motors...\n");
    
    // Dispose before initializing in case we had been initialized before
    dispose();

    Motors::driver = driver;

    driver->initialize();
}

void Motors::dispose() {
    if (driver) {
        // first make sure the throttles are all at 0
        driver->setThrottles(0.0, 0.0, 0.0, 0.0);
        
        // "initialize" may be called with an inline new object, or using the factory, so we should
        // clean up the driver instance here
        delete driver;
        driver = NULL;
    }
}

void Motors::setThrottles(double th0, double th1, double th2, double th3) {
    setThrottlesDangerous(
        std::min(th0, MOTOR_MAX_THROTTLE),
        std::min(th1, MOTOR_MAX_THROTTLE),
        std::min(th2, MOTOR_MAX_THROTTLE),
        std::min(th3, MOTOR_MAX_THROTTLE)
    );
}

void Motors::setThrottlesDangerous(double th0, double th1, double th2, double th3) {
    if (!driver) return;

    th0 = clamp(th0, 0.0, 1.0);
    th1 = clamp(th1, 0.0, 1.0);
    th2 = clamp(th2, 0.0, 1.0);
    th3 = clamp(th3, 0.0, 1.0);

    driver->setThrottles(th0, th1, th2, th3);
}

void Motors::setThrottle(uint8_t motor, double throttle) {
    setThrottleDangerous(motor, std::min(throttle, MOTOR_MAX_THROTTLE));
}

void Motors::setThrottleDangerous(uint8_t motor, double throttle) {
    if (!driver) return;
    if (motor < 0 || motor >= 4) return;

    throttle = clamp(throttle, 0.0, 1.0);

    driver->setThrottle(motor, throttle);
}

/* #endregion Public */

/* #region Private */

MotorDriver *Motors::driver = NULL;

/* #endregion Private */