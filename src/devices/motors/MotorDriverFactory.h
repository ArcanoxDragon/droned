#pragma once

#include <string>

#include "MotorDriver.h"

using string = std::string;

class MotorDriverFactory {
public:
    static MotorDriver *createDriver(const string name);
};