#include <cstring>
#include <math.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>

#include "../../config.h"
#include "../../mathutil.h"
#include "../../output.h"
#include "Motors.h"
#include "Pca9685MotorDriver.h"

#define I2C_REG_MODE1       0x00
#define I2C_REG_CH0_ON_L    0x06
#define I2C_REG_CH0_ON_H    0x07
#define I2C_REG_CH0_OFF_L   0x08
#define I2C_REG_CH0_OFF_H   0x09
#define I2C_REG_CH1_ON_L    0x0A
#define I2C_REG_CH1_ON_H    0x0B
#define I2C_REG_CH1_OFF_L   0x0C
#define I2C_REG_CH1_OFF_H   0x0D
#define I2C_REG_CH2_ON_L    0x0E
#define I2C_REG_CH2_ON_H    0x0F
#define I2C_REG_CH2_OFF_L   0x10
#define I2C_REG_CH2_OFF_H   0x11
#define I2C_REG_CH3_ON_L    0x12
#define I2C_REG_CH3_ON_H    0x13
#define I2C_REG_CH3_OFF_L   0x14
#define I2C_REG_CH3_OFF_H   0x15
#define I2C_REG_PRE_SCALE   0xFE

#define MODE1_RESTART       0b10000000
#define MODE1_AUTOINC_REG   0b00100000
#define MODE1_SLEEP         0b00010000

#define PWM_OSC_FREQUENCY   25000000                    // 25 MHz
#define PWM_UPDATE_RATE     1500                        // 2.0 kHz
#define PWM_PERIOD          (1000000 / PWM_UPDATE_RATE) // Period of PWM updates in us
#define PWM_STEPS           4096
#define PWM_DELAY           0                           // 0 us delay
#define PWM_PULSE_WIDTH_MIN 125                         // 125 us minimum pulse width
#define PWM_PULSE_WIDTH_MAX 250                         // 250 us maximum pulse width

/* #region Public */

Pca9685MotorDriver::Pca9685MotorDriver(uint8_t i2cAddress) {
    this->i2cAddress = i2cAddress;

    memset(this->delayStepsCache, 0, sizeof(this->delayStepsCache));
    memset(this->offStepsCache, 0, sizeof(this->offStepsCache));
}

Pca9685MotorDriver::~Pca9685MotorDriver() { }

void Pca9685MotorDriver::initialize() {
    LOG_VERBOSE("Initializing PCA9685 motor driver...\n");

    int result = wiringPiI2CSetup(this->i2cAddress);

    if (result < 0) {
        throw "Error initializing PCA9685 Motor Driver";
    }

    this->fdDriver = result;

    // Reset the driver
    wiringPiI2CWriteReg8(this->fdDriver, I2C_REG_MODE1, MODE1_AUTOINC_REG);
    delay(10);

    // Enable the driver
    LOG_VERBOSE("\tEnabling driver oscillator at 50 Hz\n");
    this->setFrequency(PWM_UPDATE_RATE);
    this->enableDriver(true);

    for (int motor = 0; motor < NUM_MOTORS; motor++) {
        LOG_VERBOSE("Setting motor %d throttle to 0%%\n", motor);
        this->setThrottle(motor, 0.0);
    }
}

void Pca9685MotorDriver::setThrottles(double th0, double th1, double th2, double th3) {
    pca9685_i2c_4ch_t i2cPacket;

    // Register address (start with ch0)
    i2cPacket.reg = I2C_REG_CH0_ON_L;

    // Timing data
    this->calculateSteps(th0, &i2cPacket.delayStepsCh0, &i2cPacket.offStepsCh0);
    this->calculateSteps(th1, &i2cPacket.delayStepsCh1, &i2cPacket.offStepsCh1);
    this->calculateSteps(th2, &i2cPacket.delayStepsCh2, &i2cPacket.offStepsCh2);
    this->calculateSteps(th3, &i2cPacket.delayStepsCh3, &i2cPacket.offStepsCh3);

    // Fill the cache
    this->delayStepsCache[0] = i2cPacket.delayStepsCh0;
    this->delayStepsCache[1] = i2cPacket.delayStepsCh1;
    this->delayStepsCache[2] = i2cPacket.delayStepsCh2;
    this->delayStepsCache[3] = i2cPacket.delayStepsCh3;
    this->offStepsCache[0] = i2cPacket.offStepsCh0;
    this->offStepsCache[1] = i2cPacket.offStepsCh1;
    this->offStepsCache[2] = i2cPacket.offStepsCh2;
    this->offStepsCache[3] = i2cPacket.offStepsCh3;

    // Write the data in a burst
    write(this->fdDriver, &i2cPacket, sizeof(i2cPacket));
}

void Pca9685MotorDriver::setThrottle(uint8_t motor, double throttle) {
    uint8_t baseRegister;

    switch (motor) {
        case 0:
            baseRegister = I2C_REG_CH0_ON_L;
            break;
        case 1:
            baseRegister = I2C_REG_CH1_ON_L;
            break;
        case 2:
            baseRegister = I2C_REG_CH2_ON_L;
            break;
        default:
            baseRegister = I2C_REG_CH3_ON_L;
            break;
    }

    pca9685_i2c_1ch_t i2cPacket;

    // Register address
    i2cPacket.reg = baseRegister;

    // Timing data
    this->calculateSteps(throttle, &i2cPacket.delaySteps, &i2cPacket.offSteps);

    // If no change will be written, don't perform a write
    if (i2cPacket.delaySteps == this->delayStepsCache[motor] && i2cPacket.offSteps == this->offStepsCache[motor])
        return;

    this->delayStepsCache[motor] = i2cPacket.delaySteps;
    this->offStepsCache[motor] = i2cPacket.offSteps;

    // Write the data in a burst
    write(this->fdDriver, &i2cPacket, sizeof(i2cPacket));
}

/* #endregion Public */

/* #region Private */

void Pca9685MotorDriver::calculateSteps(double throttle, uint16_t *delaySteps, uint16_t *offSteps) {
    // Pulse width in us
    double pulseWidth = map(throttle, 0.0, 1.0, PWM_PULSE_WIDTH_MIN, PWM_PULSE_WIDTH_MAX);

    // To calculate steps, we take (width / period) * 4095. "width / period" gives us percentage of PWM window,
    // and we multiply that by the number of steps (max value is 4095 because there are 4096 steps including 0)
    (*delaySteps) = (PWM_DELAY / PWM_PERIOD) * (PWM_STEPS - 1);
    (*offSteps) = (*delaySteps) + (uint16_t) ((pulseWidth / (double) PWM_PERIOD) * (PWM_STEPS - 1));

    // Mask values to 4096
    (*delaySteps) &= 0xFFF;
    (*offSteps) &= 0xFFF;
}

bool Pca9685MotorDriver::isEnabled() {
    uint8_t mode = wiringPiI2CReadReg8(this->fdDriver, I2C_REG_MODE1);

    return (mode & MODE1_SLEEP) == 0;
}

void Pca9685MotorDriver::enableDriver(bool enable) {
    uint8_t mode = wiringPiI2CReadReg8(this->fdDriver, I2C_REG_MODE1);

    if (enable) {
        // Unset sleep bit
        mode &= ~MODE1_SLEEP;
    } else {
        // Set sleep bit
        mode |= MODE1_SLEEP;
    }

    wiringPiI2CWriteReg8(this->fdDriver, I2C_REG_MODE1, mode);
    delay(5);

    if (enable) {
        mode = wiringPiI2CReadReg8(this->fdDriver, I2C_REG_MODE1);
        mode |= MODE1_RESTART;

        wiringPiI2CWriteReg8(this->fdDriver, I2C_REG_MODE1, mode);
    }
}

void Pca9685MotorDriver::setFrequency(double frequency) {
    double prescaleValue = (double) PWM_OSC_FREQUENCY;

    prescaleValue /= 0xFFF; // bit depth of output
    prescaleValue /= frequency;
    prescaleValue = round(prescaleValue) - 1;

    bool wasEnabled = this->isEnabled();

    if (wasEnabled) {
        // Put the driver to sleep before writing the new frequency
        this->enableDriver(false);
    }

    wiringPiI2CWriteReg8(this->fdDriver, I2C_REG_PRE_SCALE, ((int) prescaleValue) & 0xFF);

    if (wasEnabled) {
        // Re-enable the driver
        this->enableDriver(true);
    }
}

/* #endregion Private */