#include <cstdio>
#include <cstring>
#include <errno.h>
#include <fstream>
#include <stdio.h>

#include "../../json/json.h"
#include "../../ErrorMessage.h"
#include "OffsetCalibration.h"

using ifstream = std::ifstream;
using ofstream = std::ofstream;
using ios = std::ios;

#define CALIBRATION_FILE_NAME   "offset_calibration.json"

bool OffsetCalibration::savedCalibrationExists() {
    ifstream calibrationStream(CALIBRATION_FILE_NAME);

    return calibrationStream.good();
}

void OffsetCalibration::deleteCalibration() {
    std::remove(CALIBRATION_FILE_NAME);
}

bool OffsetCalibration::loadSavedCalibration(offset_calibration_t &data) {
    ifstream calibrationStream(CALIBRATION_FILE_NAME);

    if (!calibrationStream.good()) {
        fprintf(stderr, "Could not load offset calibration\n");
        return false;
    }

    json calibration;

    try {
        calibrationStream >> calibration;
    } catch (nlohmann::detail::exception &exception) {
        throw std::runtime_error(ErrorMessage() << "JSON error: " << exception.what());
    }

    data.pitchOffset = calibration.value("pitchOffset", 0.0);
    data.rollOffset = calibration.value("rollOffset", 0.0);

    return true;
}

void OffsetCalibration::saveCalibration(const offset_calibration_t &data) {
    json calibration;

    calibration["pitchOffset"] = data.pitchOffset;
    calibration["rollOffset"] = data.rollOffset;

    ofstream calibrationStream(CALIBRATION_FILE_NAME, ios::trunc);

    if (!calibrationStream.good()) {
        throw std::runtime_error(ErrorMessage() << "Could not save offset calibration: " << CALIBRATION_FILE_NAME);
    }

    calibrationStream << calibration;
}