#include <cstring>
#include <stddef.h>
#include <string>

#include "../../output.h"
#include "../../settings.h"
#include "Accelerometer.h"
#include "AccelerometerDriver.h"
#include "AccelerometerDriverFactory.h"

using string = std::string;

#define ORIENTATION_AVERAGE_ALPHA       0.25

/* #region Public */

void Accelerometer::initialize() {
    json settings = Settings::get();
    string driverName = settings["accelerometerDriver"].get<string>();

    initialize(AccelerometerDriverFactory::createDriver(driverName));
}

void Accelerometer::initialize(AccelerometerDriver *driver) {
    LOG_VERBOSE("Initializing Accelerometer...\n");

    // Dispose before initializing in case we had been initialized before
    dispose();

    // Load calibration data
    reloadOffsetCalibration();

    Accelerometer::driver = driver;

    driver->initialize();
}

void Accelerometer::reloadOffsetCalibration() {
    bool loaded = false;

    if (OffsetCalibration::savedCalibrationExists()) {
        printf("Loading orientation offset calibration...");

        if (OffsetCalibration::loadSavedCalibration(offsetCalibration)) {
            printf("Success.\n");
            loaded = true;
        } else {
            printf("Error. Could not load offset calibration.\n");
        }
    }

    if (!loaded) {
        // Reset it to 0
        memset(&offsetCalibration, 0, sizeof(offsetCalibration));
    }
}

void Accelerometer::dispose() {
    if (driver) {
        // "initialize" may be called with an inline new object, or using the factory, so we should
        // clean up the driver instance here
        delete driver;
        driver = NULL;
    }
}

void Accelerometer::update() {
    if (!driver) return;

    // Load data from driver
    driver->getOrientation(cachedOrientation);
    driver->getAngularVelocity(cachedAngularVelocity);
    driver->getAcceleration(cachedAcceleration);
    
    // Apply filters
    cachedOrientation.pitch = pitchAverage.get(cachedOrientation.pitch);
    cachedOrientation.roll = rollAverage.get(cachedOrientation.roll);
    cachedOrientation.heading = headingAverage.get(cachedOrientation.heading);

    // Apply orientation offset
    cachedOrientation.pitch -= offsetCalibration.pitchOffset;
    cachedOrientation.roll -= offsetCalibration.rollOffset;
}

void Accelerometer::getOrientation(angles_t &data) {
    memcpy(&data, &cachedOrientation, sizeof(angles_t));
}

void Accelerometer::getAngularVelocity(angles_t &data) {
    memcpy(&data, &cachedAngularVelocity, sizeof(angles_t));
}

void Accelerometer::getAcceleration(vector_t &data) {
    memcpy(&data, &cachedAcceleration, sizeof(vector_t));
}

/* #endregion Public */

/* #region Private */

AccelerometerDriver *Accelerometer::driver = NULL;
offset_calibration_t Accelerometer::offsetCalibration = offset_calibration_t();

ExponentialMovingAverage Accelerometer::pitchAverage = ExponentialMovingAverage(ORIENTATION_AVERAGE_ALPHA);
ExponentialMovingAverage Accelerometer::rollAverage = ExponentialMovingAverage(ORIENTATION_AVERAGE_ALPHA);
ExponentialMovingAverage Accelerometer::headingAverage = ExponentialMovingAverage(ORIENTATION_AVERAGE_ALPHA);

angles_t Accelerometer::cachedOrientation = angles_t();
angles_t Accelerometer::cachedAngularVelocity = angles_t();
vector_t Accelerometer::cachedAcceleration = vector_t();

/* #endregion Private */