#pragma once

#include <math.h>
#include <stdint.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>

#include "../../utils.h"
#include "ImuCalibration.h"
#include "AccelerometerDriver.h"

/* #region Enums and Data Structures */

/* #region Status */

enum bno055_status_t {
    bno055_status_idle,
    bno055_status_error,
    bno055_status_initializing_peripherals,
    bno055_status_system_initialization,
    bno055_status_selftest,
    bno055_status_fusion_running,
    bno055_status_running_no_fusion,
};

enum bno055_error_t {
    bno055_error_none,
    bno055_error_peripheral_init_error,
    bno055_error_system_init_error,
    bno055_error_self_test_failed,
    bno055_error_register_value_oor,
    bno055_error_register_addr_oor,
    bno055_error_register_write_error,
    bno055_error_low_power_na,
    bno055_error_acc_not_available,
    bno055_error_fusion_config_error,
    bno055_error_sensor_config_error,
};

/* #endregion Status */

/* #region Calibration */

enum bno055_axis_t {
    bno055_axis_x,
    bno055_axis_y,
    bno055_axis_z,
};

enum bno055_axis_sign_t {
    bno055_axis_positive,
    bno055_axis_negative,
};

struct bno055_calib_stat_t {
    uint8_t sys, gyro, acc, mag;
};

/* #endregion Calibration */

/* #region Data */

struct __attribute__((packed)) bno055_vector_t {
    int16_t rawX, rawY, rawZ;
    double x, y, z;
};

struct __attribute__((packed)) bno055_euler_t {
    int16_t rawHeading, rawRoll, rawPitch;
    double heading, roll, pitch;
};

struct bno055_quaternion_t {
    int16_t rawW, rawX, rawY, rawZ;
    double w, x, y, z;
};

/* #endregion Data */

/* #region Parameters/Config */

/* #region Power Mode */

enum bno055_powermode_t {
    bno055_powermode_normal = 0b00,
    bno055_powermode_low_power = 0b01,
    bno055_powermode_suspend = 0b10,
};

/* #endregion Power Mode */

/* #region Operation Mode */

enum bno055_opermode_t {
    bno055_opermode_config = 0b0000,
    bno055_opermode_acconly = 0b0001,
    bno055_opermode_magonly = 0b0010,
    bno055_opermode_gyroonly = 0b0011,
    bno055_opermode_accmag = 0b0100,
    bno055_opermode_accgyro = 0b0101,
    bno055_opermode_maggyro = 0b0110,
    bno055_opermode_amg = 0b0111,
    bno055_opermode_imu = 0b1000,
    bno055_opermode_compass = 0b1001,
    bno055_opermode_m4g = 0b1010,
    bno055_opermode_ndof_fmc_off = 0b1011,
    bno055_opermode_ndof = 0b1100,
};

/* #endregion Operation Mode */

/* #region Accelerometer Config */

enum bno055_accrange_t {
    bno055_accrange_2g,
    bno055_accrange_4g,
    bno055_accrange_8g,
    bno055_accrange_16g,
};

enum bno055_accbandwidth_t {
    bno055_accbandwidth_7_81_Hz,
    bno055_accbandwidth_15_63_Hz,
    bno055_accbandwidth_31_25_Hz,
    bno055_accbandwidth_62_5_Hz,
    bno055_accbandwidth_125_Hz,
    bno055_accbandwidth_250_Hz,
    bno055_accbandwidth_500_Hz,
    bno055_accbandwidth_1000_Hz,
};

enum bno055_accopermode_t {
    bno055_accopermode_normal,
    bno055_accopermode_suspend,
    bno055_accopermode_lowpower1,
    bno055_accopermode_standby,
    bno055_accopermode_lowpower2,
    bno055_accopermode_deepsuspend,
};

/* #endregion Accelerometer Config */

/* #region Gyroscope Config */

enum bno055_gyrorange_t {
    bno055_gyrorange_2000dps,
    bno055_gyrorange_1000dps,
    bno055_gyrorange_500dps,
    bno055_gyrorange_250dps,
    bno055_gyrorange_125dps,
};

enum bno055_gyrobandwidth_t {
    bno055_gyrobandwidth_523_Hz,
    bno055_gyrobandwidth_230_Hz,
    bno055_gyrobandwidth_116_Hz,
    bno055_gyrobandwidth_47_Hz,
    bno055_gyrobandwidth_23_Hz,
    bno055_gyrobandwidth_12_Hz,
    bno055_gyrobandwidth_64_Hz,
    bno055_gyrobandwidth_32_Hz,
};

enum bno055_gyroopermode_t {
    bno055_gyroopermode_normal,
    bno055_gyroopermode_fastpowerup,
    bno055_gyroopermode_deepsuspend,
    bno055_gyroopermode_suspend,
    bno055_gyroopermode_advancedpowersave,
};

/* #endregion Gyroscope Config */

/* #region Magnetometer Config */

enum bno055_magdatarate_t {
    bno055_magdatarate_2_Hz,
    bno055_magdatarate_6_Hz,
    bno055_magdatarate_8_Hz,
    bno055_magdatarate_10_Hz,
    bno055_magdatarate_15_Hz,
    bno055_magdatarate_20_Hz,
    bno055_magdatarate_25_Hz,
    bno055_magdatarate_30_Hz,
};

enum bno055_magopermode_t {
    bno055_magopermode_lowpower,
    bno055_magopermode_regular,
    bno055_magopermode_enhregular,
    bno055_magopermode_highaccuracy,
};

enum bno055_magpowermode_t {
    bno055_magpowermode_normal,
    bno055_magpowermode_sleep,
    bno055_magpowermode_suspend,
    bno055_magpowermode_forcemode,
};

/* #endregion Magnetometer Config */

/* #region Output Format */

enum bno055_acc_units_t {
    bno055_acc_units_ms2,
    bno055_acc_units_mg,
};

enum bno055_angrate_units_t {
    bno055_angrate_units_dps,
    bno055_angrate_units_rps,
};

enum bno055_ang_units_t {
    bno055_ang_units_deg,
    bno055_ang_units_rad,
};

enum bno055_temp_units_t {
    bno055_temp_units_celsius,
    bno055_temp_units_fahrenheit,
};

enum bno055_data_format_t {
    bno055_data_format_windows,
    bno055_data_format_android,
};

/* #endregion Output Format */

/* #endregion Parameters/Config */

/* #endregion Enums and Data Structures */

class ImuAccelerometerDriver : public AccelerometerDriver {
public:
    static ImuAccelerometerDriver *get();

    ImuAccelerometerDriver(uint8_t i2cAddress);
    ~ImuAccelerometerDriver();

    void initialize();
    void getOrientation(angles_t &data);
    void getAngularVelocity(angles_t &data);
    void getAcceleration(vector_t &data);

    /* #region Status */
    
    bno055_status_t getStatus();
    const char* getStatusMessage();
    bno055_error_t getError();
    const char* getErrorMessage();
    
    /* #endregion Status */

    /* #region Calibration */

    void setAxisMap(bno055_axis_t xAxisMap, bno055_axis_sign_t xAxisSign, bno055_axis_t yAxisMap, bno055_axis_sign_t yAxisSign, bno055_axis_t zAxisMap, bno055_axis_sign_t zAxisSign);

    void getCalibrationStatus(bno055_calib_stat_t &data);
    void getCalibrationData(bno055_calibration_t &data);
    void setCalibrationData(const bno055_calibration_t &data);

    /* #endregion Calibration */

    /* #region Data */

    /* #region Non-Fusion */

    void getAmgData(bno055_vector_t &accelerometer, bno055_vector_t &magnetometer, bno055_vector_t &gyrometer);
    void getAccelerationData(bno055_vector_t &data);
    void getMagnetometerData(bno055_vector_t &data);
    void getGyrometerData(bno055_vector_t &data);

    /* #endregion Non-Fusion */

    /* #region Fusion */

    void getImuData(bno055_euler_t &orientation, bno055_vector_t &linearAcceleration, bno055_vector_t &gravity);
    void getOrientationDataEuler(bno055_euler_t &data);
    void getOrientationDataQuaternion(bno055_quaternion_t &data);
    void getLinearAccelerationData(bno055_vector_t &data);
    void getGravityVectorData(bno055_vector_t &data);

    /* #endregion Fusion */

    int8_t getTemperature();

    /* #endregion Data */

    void resetDevice();

    /* #region Parameters */

    bno055_powermode_t getPowerMode();
    void setPowerMode(bno055_powermode_t powermode);
    bno055_opermode_t getOperationMode();
    void setOperationMode(bno055_opermode_t opermode);

    bno055_accrange_t getAccelerometerRange(bool skipCache = false);
    void setAccelerometerRange(bno055_accrange_t accrange);
    bno055_accbandwidth_t getAccelerometerBandwidth(bool skipCache = false);
    void setAccelerometerBandwidth(bno055_accbandwidth_t accbandwidth);
    bno055_accopermode_t getAccelerometerOperationMode(bool skipCache = false);
    void setAccelerometerOperationMode(bno055_accopermode_t accopermode);

    bno055_gyrorange_t getGyroscopeRange(bool skipCache = false);
    void setGyroscopeRange(bno055_gyrorange_t gyrorange);
    bno055_gyrobandwidth_t getGyroscopeBandwidth(bool skipCache = false);
    void setGyroscopeBandwidth(bno055_gyrobandwidth_t gyrobandwidth);
    bno055_gyroopermode_t getGyroscopeOperationMode(bool skipCache = false);
    void setGyroscopeOperationMode(bno055_gyroopermode_t gyroopermode);

    bno055_magdatarate_t getMagnetometerDataRage(bool skipCache = false);
    void setMagnetometerDataRage(bno055_magdatarate_t magdatarate);
    bno055_magopermode_t getMagnetometerOperationMode(bool skipCache = false);
    void setMagnetometerOperationMode(bno055_magopermode_t magopermode);
    bno055_magpowermode_t getMagnetometerPowerMode(bool skipCache = false);
    void setMagnetometerPowerMode(bno055_magpowermode_t magpowermode);

    bno055_acc_units_t getAccelerationUnits(bool skipCache = false);
    void setAccelerationUnits(bno055_acc_units_t accUnits);
    bno055_angrate_units_t getAngularRateUnits(bool skipCache = false);
    void setAngularRateUnits(bno055_angrate_units_t angrateUnits);
    bno055_ang_units_t getAngleUnits(bool skipCache = false);
    void setAngleUnits(bno055_ang_units_t angUnits);
    bno055_temp_units_t getTemperatureUnits(bool skipCache = false);
    void setTemperatureUnits(bno055_temp_units_t tempUnits);
    bno055_data_format_t getDataFormat(bool skipCache = false);
    void setDataFormat(bno055_data_format_t dataFormat);

    /* #endregion Parameters */
private:
    static ImuAccelerometerDriver *instance;

    uint8_t i2cAddress = 0;
    int fdImu = -1;
    bool hasCachedAccrange = false;
    bno055_accrange_t cachedAccrange;
    bool hasCachedAccbandwidth = false;
    bno055_accbandwidth_t cachedAccbandwidth;
    bool hasCachedAccopermode = false;
    bno055_accopermode_t cachedAccopermode;
    bool hasCachedGyrorange = false;
    bno055_gyrorange_t cachedGyrorange;
    bool hasCachedGyrobandwidth = false;
    bno055_gyrobandwidth_t cachedGyrobandwidth;
    bool hasCachedGyroopermode = false;
    bno055_gyroopermode_t cachedGyroopermode;
    bool hasCachedMagdatarate = false;
    bno055_magdatarate_t cachedMagdatarate;
    bool hasCachedMagopermode = false;
    bno055_magopermode_t cachedMagopermode;
    bool hasCachedMagpowermode = false;
    bno055_magpowermode_t cachedMagpowermode;
    bool hasCachedAccUnits = false;
    bno055_acc_units_t cachedAccUnits;
    bool hasCachedAngrateUnits = false;
    bno055_angrate_units_t cachedAngrateUnits;
    bool hasCachedAngUnits = false;
    bno055_ang_units_t cachedAngUnits;
    bool hasCachedTempUnits = false;
    bno055_temp_units_t cachedTempUnits;
    bool hasCachedDataFormat = false;
    bno055_data_format_t cachedDataFormat;

    void setPage(uint8_t page);

    template <typename T>
    T getDeviceParameter(uint8_t page, uint8_t paramRegister, uint8_t paramMask, uint8_t paramShift = 0) {
        if (page != 0) {
            // Select Page 1 first
            this->setPage(1);
        }

        uint8_t currentValue = wiringPiI2CReadReg8(this->fdImu, paramRegister);

        if (page != 0) {
            // Restore Page 0
            this->setPage(0);
        }

        return static_cast<T>((currentValue & paramMask) >> paramShift);
    }

    template <typename T>
    void setDeviceParameter(uint8_t page, uint8_t paramRegister, uint8_t paramMask, uint8_t paramShift, T value) {
        if (page != 0) {
            // Select Page 1 first
            this->setPage(1);
        }

        bno055_opermode_t prevOperMode = this->getOperationMode();

        this->setOperationMode(bno055_opermode_config);
        delay(1);

        uint8_t currentValue = wiringPiI2CReadReg8(this->fdImu, paramRegister);
        uint8_t newValue = setBits<uint8_t>(currentValue, paramMask, ((uint8_t) value) << paramShift);

        wiringPiI2CWriteReg8(this->fdImu, paramRegister, newValue);

        this->setOperationMode(prevOperMode);
        delay(1);

        if (page != 0) {
            // Restore Page 0
            this->setPage(0);
        }
    }

    template <typename T>
    void setDeviceParameter(uint8_t paramRegister, uint8_t paramMask, T value) {
        this->setDeviceParameter(paramRegister, paramMask, 0, value);
    }
    
    /* #region LSB Calculation */
    
    double getAccelerometerLsb();
    double getGyrometerLsb();
    double getTemperatureLsb();
    double getEulerOrientationLsb();
    
    /* #endregion LSB Calculation */

    void readVectors(bno055_vector_t **data, uint8_t numVectors, uint8_t startingRegister, const double *lsbs);
    void readVector(bno055_vector_t &data, uint8_t startingRegister, double lsb);
    void readQuaternion(bno055_quaternion_t &data, uint8_t startingRegister);
};