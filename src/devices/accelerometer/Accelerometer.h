#pragma once

#include <stdint.h>

#include "../../types.h"
#include "../../filters/ExponentialMovingAverage.h"
#include "OffsetCalibration.h"

class AccelerometerDriver;

class Accelerometer {
public:
    static void initialize();
    static void initialize(AccelerometerDriver *driver);
    static void reloadOffsetCalibration();
    static void dispose();

    static void update();
    static void getOrientation(angles_t &data);
    static void getAngularVelocity(angles_t &data);
    static void getAcceleration(vector_t &data);

private:
    static AccelerometerDriver *driver;
    static offset_calibration_t offsetCalibration;
    
    static ExponentialMovingAverage pitchAverage;
    static ExponentialMovingAverage rollAverage;
    static ExponentialMovingAverage headingAverage;
    
    static angles_t cachedOrientation;
    static angles_t cachedAngularVelocity;
    static vector_t cachedAcceleration;
};