#include <cstring>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "ImuCalibration.h"

#define CALIBRATION_FILE_NAME   "imu_calibration.dat"

bool ImuCalibration::savedCalibrationExists() {
    int result = open(CALIBRATION_FILE_NAME, O_RDONLY);

    if (result >= 0) {
        close(result);
    }

    return result >= 0;
}

void ImuCalibration::deleteCalibration() {
    unlink(CALIBRATION_FILE_NAME);
}

bool ImuCalibration::loadSavedCalibration(bno055_calibration_t &data) {
    int fd = open(CALIBRATION_FILE_NAME, O_RDONLY);

    if (fd < 0)
        return false;

    char buf[sizeof(bno055_calibration_t)];
    int numRead = read(fd, buf, sizeof(bno055_calibration_t));

    close(fd);

    if (numRead < sizeof(bno055_calibration_t)) {
        return false;
    }

    memcpy(&data, buf, sizeof(bno055_calibration_t));
    return true;
}

bool ImuCalibration::saveCalibration(const bno055_calibration_t &data) {
    int fd = open(CALIBRATION_FILE_NAME, O_WRONLY | O_CREAT | O_TRUNC, 0644);

    if (fd < 0)
        return false;

    int numWritten = write(fd, &data, sizeof(bno055_calibration_t));

    close(fd);

    return numWritten >= sizeof(bno055_calibration_t);
}