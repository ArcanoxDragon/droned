#pragma once

#include "Accelerometer.h"

class AccelerometerDriver {
public:
    virtual ~AccelerometerDriver() { }

    virtual void initialize() = 0;
    virtual void getOrientation(angles_t &data) = 0;
    virtual void getAngularVelocity(angles_t &data) = 0;
    virtual void getAcceleration(vector_t &data) = 0;

protected:
    AccelerometerDriver() { }
};