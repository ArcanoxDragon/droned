#pragma once

#include <stdint.h>

struct __attribute__((packed)) offset_calibration_t {
    double pitchOffset, rollOffset;
};

class OffsetCalibration {
public:
    static bool savedCalibrationExists();
    static void deleteCalibration();
    static bool loadSavedCalibration(offset_calibration_t &data);
    static void saveCalibration(const offset_calibration_t &data);
};