#pragma once

#include <string>

#include "AccelerometerDriver.h"

using string = std::string;

class AccelerometerDriverFactory {
public:
    static AccelerometerDriver *createDriver(const string name);
};