#include <math.h>
#include <wiringPiI2C.h>

#include "OldAccelerometerDriver.h"

/* #region Public */

OldAccelerometerDriver::OldAccelerometerDriver(uint8_t i2cAddress) {
    this->i2cAddress = i2cAddress;
}

OldAccelerometerDriver::~OldAccelerometerDriver() { }

void OldAccelerometerDriver::initialize() {
    int result = wiringPiI2CSetup(this->i2cAddress);

    if (result < 0)
        throw "Error initializing I2C Accelerometer device";

    this->fdAccelerometer = result;
    this->setDeviceMode();
}

void OldAccelerometerDriver::getOrientation(angles_t &data) {
    vector_t accel;

    getAcceleration(accel);

    double m = sqrtf(powf(accel.x, 2) + powf(accel.y, 2) + powf(accel.z, 2));

    data.pitch = 90 - acosf(accel.y / m) * 180 / (double) M_PI;
    data.roll = 90 - acosf(accel.x / m) * 180 / (double) M_PI;
    data.heading = 0.0;

    return;
}

void OldAccelerometerDriver::getAngularVelocity(angles_t &data) {
    data.pitch = 0.0;
    data.roll = 0.0;
    data.heading = 0.0;
}

void OldAccelerometerDriver::getAcceleration(vector_t &data) {
    int16_t rawX, rawY, rawZ;

    if (!updateRaw(rawX, rawY, rawZ)) {
        return;
    }

    const int shift = 6;
    const double gain = 0.0039f;

    data.x = (double) (rawX >> shift) * gain * GRAVITY_EARTH;
    data.y = (double) (rawY >> shift) * gain * GRAVITY_EARTH;
    data.z = (double) (rawZ >> shift) * gain * GRAVITY_EARTH;

    return;
}

/* #endregion Public */

/* #region Private */

bool OldAccelerometerDriver::updateRaw(int16_t &x, int16_t &y, int16_t &z) {
    int32_t xLow = wiringPiI2CReadReg8(this->fdAccelerometer, I2C_REG_X_L);
    int32_t xHigh = wiringPiI2CReadReg8(this->fdAccelerometer, I2C_REG_X_H);
    int32_t yLow = wiringPiI2CReadReg8(this->fdAccelerometer, I2C_REG_Y_L);
    int32_t yHigh = wiringPiI2CReadReg8(this->fdAccelerometer, I2C_REG_Y_H);
    int32_t zLow = wiringPiI2CReadReg8(this->fdAccelerometer, I2C_REG_Z_L);
    int32_t zHigh = wiringPiI2CReadReg8(this->fdAccelerometer, I2C_REG_Z_H);

    if (xLow < 0 || xHigh < 0 || yLow < 0 || yHigh < 0 || zLow < 0 || zHigh < 0) {
        return false;
    }

    x = (int16_t) (xLow | (xHigh << 8));
    y = (int16_t) (yLow | (yHigh << 8));
    z = (int16_t) (zLow | (zHigh << 8));

    return true;
}

void OldAccelerometerDriver::setDeviceMode() {
    uint8_t refreshRateBits = 0b0111; // 400 Hz
    uint8_t modeBits = 0b0111;        // Normal mode, XYZ enable
    uint8_t ctrlReg1 = refreshRateBits << 4 | modeBits;

    // Write control register 1
    wiringPiI2CWriteReg8(this->fdAccelerometer, I2C_REG_CTRL_1, ctrlReg1);
}

/* #endregion Private */