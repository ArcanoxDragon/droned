#include <cstring>
#include <math.h>
#include <stdexcept>
#include <stdio.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>

#include "../../ErrorMessage.h"
#include "ImuAccelerometerDriver.h"

/* #region BNO055 Constants */

// Uncomment this if the BNO055 sensor is physically flipped around the Y axis
// #define BNO055_SENSOR_FLIPPED

#define BNO055_PAGE_ID_REG              0x07
#define BNO055_SYS_STATUS_REG           0x39
#define BNO055_SYS_ERROR_REG            0x3A

#define BNO055_AXIS_MAP_CONFIG_REG      0x41
#define BNO055_AXIS_MAP_X_MASK          0b00000011
#define BNO055_AXIS_MAP_X_SHIFT         0
#define BNO055_AXIS_MAP_Y_MASK          0b00001100
#define BNO055_AXIS_MAP_Y_SHIFT         2
#define BNO055_AXIS_MAP_Z_MASK          0b00110000
#define BNO055_AXIS_MAP_Z_SHIFT         4
#define BNO055_AXIS_MAP_SIGN_REG        0x42
#define BNO055_AXIS_MAP_SIGN_X_MASK     0b00000100
#define BNO055_AXIS_MAP_SIGN_X_SHIFT    2
#define BNO055_AXIS_MAP_SIGN_Y_MASK     0b00000010
#define BNO055_AXIS_MAP_SIGN_Y_SHIFT    1
#define BNO055_AXIS_MAP_SIGN_Z_MASK     0b00000001
#define BNO055_AXIS_MAP_SIGN_Z_SHIFT    0

#define BNO055_CALIB_STAT_REG           0x35
#define BNO055_CALIB_STAT_MAG_MASK      0b00000011
#define BNO055_CALIB_STAT_MAG_SHIFT     0
#define BNO055_CALIB_STAT_ACC_MASK      0b00001100
#define BNO055_CALIB_STAT_ACC_SHIFT     2
#define BNO055_CALIB_STAT_GYR_MASK      0b00110000
#define BNO055_CALIB_STAT_GYR_SHIFT     4
#define BNO055_CALIB_STAT_SYS_MASK      0b11000000
#define BNO055_CALIB_STAT_SYS_SHIFT     6

#define BNO055_CALIB_ACC_OFF_X_REG      0x55
#define BNO055_CALIBRATION_DATA_SIZE    22

#define BNO055_ACCELERATION_XL_REG      0x08
#define BNO055_ACCELERATION_LSB_MS2     100
#define BNO055_ACCELERATION_LSB_MG      1

#define BNO055_MAGNETOMETER_XL_REG      0x0E
#define BNO055_MAGNETOMETER_LSB         16

#define BNO055_GYROSCOPE_XL_REG         0x14
#define BNO055_GYROSCOPE_LSB_DPS        16
#define BNO055_GYROSCOPE_LSB_RPS        900

#define BNO055_LINEAR_ACCEL_XL_REG      0x28
#define BNO055_GRAVITY_VEC_XL_REG       0x2E

#define BNO055_ORIENT_EULER_HL_REG      0x1A
#define BNO055_ORIENT_EULER_LSB_DEG     16
#define BNO055_ORIENT_EULER_LSB_RAD     900

#define BNO055_ORIENT_QUAT_HL_REG       0x20

#define BNO055_QUATERNION_LSB           pow(2, 14)

#define BNO055_TEMPERATURE_REG          0x34
#define BNO055_TEMPERATURE_LSB_C        1
#define BNO055_TEMPERATURE_LSB_F        0.5

#define BNO055_SYS_TRIGGER_REG          0x3F
#define BNO055_SYS_TRIGGER_RST_SYS      0b00100000

#define BNO055_POWERMODE_REG        0x3E
#define BNO055_POWERMODE_MASK       0b00000011

#define BNO055_OPERMODE_REG         0x3D
#define BNO055_OPERMODE_MASK        0b00001111

#define BNO055_ACCCONFIG_REG        0x08

#define BNO055_ACCRANGE_REG         BNO055_ACCCONFIG_REG
#define BNO055_ACCRANGE_MASK        0b00000011
#define BNO055_ACCRANGE_SHIFT       0

#define BNO055_ACCBANDWIDTH_REG     BNO055_ACCCONFIG_REG
#define BNO055_ACCBANDWIDTH_MASK    0b00011100
#define BNO055_ACCBANDWIDTH_SHIFT   2

#define BNO055_ACCOPERMODE_REG      BNO055_ACCCONFIG_REG
#define BNO055_ACCOPERMODE_MASK     0b11100000
#define BNO055_ACCOPERMODE_SHIFT    5

#define BNO055_GYROCONFIG_REG_1     0x0A
#define BNO055_GYROCONFIG_REG_2     0x0B

#define BNO055_GYRORANGE_REG        BNO055_GYROCONFIG_REG_1
#define BNO055_GYRORANGE_MASK       0b00000111
#define BNO055_GYRORANGE_SHIFT      0

#define BNO055_GYROBANDWIDTH_REG    BNO055_GYROCONFIG_REG_1
#define BNO055_GYROBANDWIDTH_MASK   0b00111000
#define BNO055_GYROBANDWIDTH_SHIFT  3

#define BNO055_GYROOPERMODE_REG     BNO055_GYROCONFIG_REG_2
#define BNO055_GYROOPERMODE_MASK    0b00000111
#define BNO055_GYROOPERMODE_SHIFT   0

#define BNO055_MAGCONFIG_REG        0x09

#define BNO055_MAGDATARATE_REG      BNO055_MAGCONFIG_REG
#define BNO055_MAGDATARATE_MASK     0b00000111
#define BNO055_MAGDATARATE_SHIFT    0

#define BNO055_MAGOPERMODE_REG      BNO055_MAGCONFIG_REG
#define BNO055_MAGOPERMODE_MASK     0b00011000
#define BNO055_MAGOPERMODE_SHIFT    3

#define BNO055_MAGPOWERMODE_REG     BNO055_MAGCONFIG_REG
#define BNO055_MAGPOWERMODE_MASK    0b01100000
#define BNO055_MAGPOWERMODE_SHIFT   5

#define BNO055_UNITSEL_REG          0x3B

#define BNO055_ACC_UNITS_REG        BNO055_UNITSEL_REG
#define BNO055_ACC_UNITS_MASK       0b00000001
#define BNO055_ACC_UNITS_SHIFT      0

#define BNO055_ANGRATE_UNITS_REG    BNO055_UNITSEL_REG
#define BNO055_ANGRATE_UNITS_MASK   0b00000010
#define BNO055_ANGRATE_UNITS_SHIFT  1

#define BNO055_ANG_UNITS_REG        BNO055_UNITSEL_REG
#define BNO055_ANG_UNITS_MASK       0b00000100
#define BNO055_ANG_UNITS_SHIFT      2

#define BNO055_TEMP_UNITS_REG       BNO055_UNITSEL_REG
#define BNO055_TEMP_UNITS_MASK      0b00010000
#define BNO055_TEMP_UNITS_SHIFT     4

#define BNO055_DATA_FORMAT_REG      BNO055_UNITSEL_REG
#define BNO055_DATA_FORMAT_MASK     0b10000000
#define BNO055_DATA_FORMAT_SHIFT    7

const char *BNO055_STATUS_MESSAGES[] = {
    "Idle",
    "System Error",
    "Initializing Peripherals",
    "System Initialization",
    "Executing Self-Test",
    "Fusion Algorithm Running",
    "Running w/o Fusion Algorithm",
};

const char *BNO055_ERROR_MESSAGES[] = {
    "No error",
    "Peripheral initialization error",
    "System initialization error",
    "Self-test result failed",
    "Register map value out-of-range",
    "Register map address out-of-range",
    "Register map write error",
    "BNO low power mode not available",
    "Accelerometer power mode not available",
    "Fusion algorithm configuration error",
    "Sensor configuration error",
};

/* #endregion BNO055 Constants */

/* #region Public */

ImuAccelerometerDriver *ImuAccelerometerDriver::get() {
    if (!instance) {
        throw "No instance of ImuAccelerometerDriver has been constructed";
    }

    return instance;
}

ImuAccelerometerDriver::ImuAccelerometerDriver(uint8_t i2cAddress) {
    instance = this;

    this->i2cAddress = i2cAddress;
}

ImuAccelerometerDriver::~ImuAccelerometerDriver() { }

void ImuAccelerometerDriver::initialize() {
    int result = wiringPiI2CSetup(this->i2cAddress);

    if (result < 0)
        throw "Error initializing I2C Accelerometer device";

    this->fdImu = result;

    // Reset the device upon initialization
    this->resetDevice();

    // Load saved calibration data, if it exists
    if (ImuCalibration::savedCalibrationExists()) {
        bno055_calibration_t calibrationData;

        printf("Loading IMU calibration data...");
        fflush(stdout);

        if (ImuCalibration::loadSavedCalibration(calibrationData)) {
            // Ensure sensor is in config mode
            this->setOperationMode(bno055_opermode_config);

            // Store the calibration data
            this->setCalibrationData(calibrationData);

            bno055_status_t status = this->getStatus();

            if (status == bno055_status_error) {
                const char *errorMessage = this->getErrorMessage();

                throw std::runtime_error(ErrorMessage() << "Error loading calibration data to device: " << errorMessage);
            }

            printf("IMU calibration data loaded and applied.\n");
        } else {
            printf("Could not load calibration data.\n");
        }
    }

    // Take the device out of configuration mode into NDOF (nine-degrees-of-freedom) mode
    this->setOperationMode(bno055_opermode_imu);
    // this->setOperationMode(bno055_opermode_amg);

    // Select Page 0 by default
    this->setPage(0);
}

void ImuAccelerometerDriver::getOrientation(angles_t &data) {
    bno055_euler_t sensorData;

    this->getOrientationDataEuler(sensorData);

    data.heading = sensorData.heading;

#ifdef BNO055_SENSOR_FLIPPED
    data.pitch = sensorData.pitch + 180.0;
    data.roll = sensorData.roll;

    if (data.pitch > 180.0) {
        data.pitch -= 360.0;
    }
#else
    data.pitch = sensorData.pitch;
    data.roll = sensorData.roll;
#endif

    /* bno055_euler_t sensorOrientation;
    bno055_vector_t sensorGravity;

    // this->getOrientationDataEuler(sensorOrientation);
    this->getAccelerationData(sensorGravity);

    double m = sqrtf(powf(sensorGravity.x, 2) + powf(sensorGravity.y, 2) + powf(sensorGravity.z, 2));

#ifdef BNO055_SENSOR_FLIPPED
    data.pitch = acosf(sensorGravity.y / m) * 180 / (double) M_PI - 90;
    data.roll = 90 - acosf(sensorGravity.x / m) * 180 / (double) M_PI;
#else
    data.pitch = 90 - acosf(sensorGravity.y / m) * 180 / (double) M_PI;
    data.roll = acosf(sensorGravity.x / m) * 180 / (double) M_PI - 90;
#endif
    data.heading = 0.0;

    if (isnan(data.pitch)) data.pitch = 0.0;
    if (isnan(data.roll)) data.roll = 0.0;
    if (isnan(data.heading)) data.heading = 0.0; */
}

void ImuAccelerometerDriver::getAngularVelocity(angles_t &data) {
    bno055_vector_t sensorData;

    getGyrometerData(sensorData);


    data.roll = sensorData.y;
    data.heading = sensorData.z;

#ifdef BNO055_SENSOR_FLIPPED
    data.pitch = -sensorData.x;
#else
    data.pitch = sensorData.x;
#endif
}

void ImuAccelerometerDriver::getAcceleration(vector_t &data) {
    bno055_vector_t sensorData;

    this->getLinearAccelerationData(sensorData);

    data.x = sensorData.x;
    data.y = sensorData.y;
    data.z = sensorData.z;
}

/* #region Status */

bno055_status_t ImuAccelerometerDriver::getStatus() {
    uint8_t regValue = wiringPiI2CReadReg8(this->fdImu, BNO055_SYS_STATUS_REG);

    return static_cast<bno055_status_t>(regValue);
}

const char *ImuAccelerometerDriver::getStatusMessage() {
    bno055_status_t status = this->getStatus();

    return BNO055_STATUS_MESSAGES[status];
}

bno055_error_t ImuAccelerometerDriver::getError() {
    uint8_t regValue = wiringPiI2CReadReg8(this->fdImu, BNO055_SYS_ERROR_REG);

    return static_cast<bno055_error_t>(regValue);
}

const char *ImuAccelerometerDriver::getErrorMessage() {
    bno055_error_t error = this->getError();

    return BNO055_ERROR_MESSAGES[error];
}

/* #endregion Status */

/* #region Calibration */

void ImuAccelerometerDriver::setAxisMap(bno055_axis_t xAxisMap, bno055_axis_sign_t xAxisSign, bno055_axis_t yAxisMap, bno055_axis_sign_t yAxisSign, bno055_axis_t zAxisMap, bno055_axis_sign_t zAxisSign) {
    uint8_t axisConfig = 0;
    uint8_t axisSign = 0;

    axisConfig |= (xAxisMap << BNO055_AXIS_MAP_X_SHIFT) & BNO055_AXIS_MAP_X_MASK;
    axisConfig |= (yAxisMap << BNO055_AXIS_MAP_Y_SHIFT) & BNO055_AXIS_MAP_Y_MASK;
    axisConfig |= (zAxisMap << BNO055_AXIS_MAP_Z_SHIFT) & BNO055_AXIS_MAP_Z_MASK;

    axisSign |= (xAxisSign << BNO055_AXIS_MAP_SIGN_X_SHIFT) & BNO055_AXIS_MAP_SIGN_X_MASK;
    axisSign |= (yAxisSign << BNO055_AXIS_MAP_SIGN_Y_SHIFT) & BNO055_AXIS_MAP_SIGN_Y_MASK;
    axisSign |= (zAxisSign << BNO055_AXIS_MAP_SIGN_Z_SHIFT) & BNO055_AXIS_MAP_SIGN_Z_MASK;

    wiringPiI2CWriteReg8(this->fdImu, BNO055_AXIS_MAP_CONFIG_REG, axisConfig);
    wiringPiI2CWriteReg8(this->fdImu, BNO055_AXIS_MAP_SIGN_REG, axisSign);
}

void ImuAccelerometerDriver::getCalibrationStatus(bno055_calib_stat_t &data) {
    uint8_t regValue = wiringPiI2CReadReg8(this->fdImu, BNO055_CALIB_STAT_REG);

    data.mag = (regValue & BNO055_CALIB_STAT_MAG_MASK) >> BNO055_CALIB_STAT_MAG_SHIFT;
    data.acc = (regValue & BNO055_CALIB_STAT_ACC_MASK) >> BNO055_CALIB_STAT_ACC_SHIFT;
    data.gyro = (regValue & BNO055_CALIB_STAT_GYR_MASK) >> BNO055_CALIB_STAT_GYR_SHIFT;
    data.sys = (regValue & BNO055_CALIB_STAT_SYS_MASK) >> BNO055_CALIB_STAT_SYS_SHIFT;
}

void ImuAccelerometerDriver::getCalibrationData(bno055_calibration_t &data) {
    // Switch to Configure mode first
    bno055_opermode_t oldMode = this->getOperationMode();

    this->setOperationMode(bno055_opermode_config);

    // Struct is packed and in the same order as the IMU's calibration registers.
    // We can read the whole thing in one go!

    static_assert(sizeof(bno055_calibration_t) == BNO055_CALIBRATION_DATA_SIZE, "Incorrect data size for bno055_calibration_t");

    // Select the lowest register
    wiringPiI2CWrite(this->fdImu, BNO055_CALIB_ACC_OFF_X_REG);

    // Read the data
    read(this->fdImu, &data, sizeof(bno055_calibration_t));

    // Restore mode
    this->setOperationMode(oldMode);
}

void ImuAccelerometerDriver::setCalibrationData(const bno055_calibration_t &data) {
    // Struct is packed and in the same order as the IMU's calibration registers.
    // We can write the whole thing in one go!

    static_assert(sizeof(bno055_calibration_t) == BNO055_CALIBRATION_DATA_SIZE, "Incorrect data size for bno055_calibration_t");

    uint8_t buf[sizeof(bno055_calibration_t)];

    // Copy the data bytes into a buffer
    memcpy(buf, &data, sizeof(bno055_calibration_t));

    // Write the bytes one-by-one into the registers
    for (uint8_t offset = 0; offset < sizeof(bno055_calibration_t); offset++) {
        uint8_t reg = BNO055_CALIB_ACC_OFF_X_REG + offset;

        wiringPiI2CWriteReg8(this->fdImu, reg, buf[offset]);
    }
}

/* #endregion Calibration */

/* #region Data */

/* #region Non-Fusion */

void ImuAccelerometerDriver::getAmgData(bno055_vector_t &accelerometer, bno055_vector_t &magnetometer, bno055_vector_t &gyrometer) {
    bno055_vector_t *vectors[] = {
        &accelerometer,
        &magnetometer,
        &gyrometer,
    };
    double lsbs[] = {
        this->getAccelerometerLsb(),
        BNO055_MAGNETOMETER_LSB,
        this->getGyrometerLsb(),
    };

    this->readVectors(vectors, 3, BNO055_ACCELERATION_XL_REG, lsbs);
}

void ImuAccelerometerDriver::getAccelerationData(bno055_vector_t &data) {
    this->readVector(data, BNO055_ACCELERATION_XL_REG, this->getAccelerometerLsb());
}

void ImuAccelerometerDriver::getMagnetometerData(bno055_vector_t &data) {
    this->readVector(data, BNO055_MAGNETOMETER_XL_REG, BNO055_MAGNETOMETER_LSB);
}

void ImuAccelerometerDriver::getGyrometerData(bno055_vector_t &data) {
    this->readVector(data, BNO055_GYROSCOPE_XL_REG, this->getGyrometerLsb());
}

/* #endregion Non-Fusion */

/* #region Fusion */

void ImuAccelerometerDriver::getImuData(bno055_euler_t &orientation, bno055_vector_t &linearAcceleration, bno055_vector_t &gravity) {
    bno055_vector_t *vectors[] = {
        &linearAcceleration,
        &gravity,
    };
    double accelerometerLsb = this->getAccelerometerLsb();
    double lsbs[] = {
        accelerometerLsb,
        accelerometerLsb,
    };

    // Sadly, the quaternion orientation registers are shoved right between the Euler registers
    // and the linear/gravity acceleration registers, so we have to first read the Euler orientation
    // as one operation and then read the linear/gravity acceleration as another. We still save one
    // read operation!
    this->getOrientationDataEuler(orientation);
    this->readVectors(vectors, 2, BNO055_LINEAR_ACCEL_XL_REG, lsbs);
}

void ImuAccelerometerDriver::getOrientationDataEuler(bno055_euler_t &data) {
    this->readVector(reinterpret_cast<bno055_vector_t &>(data), BNO055_ORIENT_EULER_HL_REG, this->getEulerOrientationLsb());
}

void ImuAccelerometerDriver::getOrientationDataQuaternion(bno055_quaternion_t &data) {
    this->readQuaternion(data, BNO055_ORIENT_QUAT_HL_REG);
}

void ImuAccelerometerDriver::getLinearAccelerationData(bno055_vector_t &data) {
    this->readVector(data, BNO055_LINEAR_ACCEL_XL_REG, this->getAccelerometerLsb());
}

void ImuAccelerometerDriver::getGravityVectorData(bno055_vector_t &data) {
    this->readVector(data, BNO055_GRAVITY_VEC_XL_REG, this->getAccelerometerLsb());
}

/* #endregion Fusion */

int8_t ImuAccelerometerDriver::getTemperature() {
    int8_t raw = wiringPiI2CReadReg8(this->fdImu, BNO055_TEMPERATURE_REG);

    return (int8_t) (raw * this->getTemperatureLsb());
}

/* #endregion Data */

void ImuAccelerometerDriver::resetDevice() {
    uint8_t regValue = wiringPiI2CReadReg8(this->fdImu, BNO055_SYS_TRIGGER_REG);

    regValue |= BNO055_SYS_TRIGGER_RST_SYS;

    wiringPiI2CWriteReg8(this->fdImu, BNO055_SYS_TRIGGER_REG, regValue);
    delay(1000);
}

/* #region Parameters */

bno055_powermode_t ImuAccelerometerDriver::getPowerMode() {
    return this->getDeviceParameter<bno055_powermode_t>(0, BNO055_POWERMODE_REG, BNO055_POWERMODE_MASK);
}

void ImuAccelerometerDriver::setPowerMode(bno055_powermode_t powermode) {
    uint8_t currentValue = wiringPiI2CReadReg8(this->fdImu, BNO055_POWERMODE_REG);
    uint8_t newValue = setBits<uint8_t>(currentValue, BNO055_POWERMODE_MASK, powermode);

    wiringPiI2CWriteReg8(this->fdImu, BNO055_POWERMODE_REG, newValue);
}

bno055_opermode_t ImuAccelerometerDriver::getOperationMode() {
    return this->getDeviceParameter<bno055_opermode_t>(0, BNO055_OPERMODE_REG, BNO055_OPERMODE_MASK);
}

void ImuAccelerometerDriver::setOperationMode(bno055_opermode_t opermode) {
    uint8_t currentValue = wiringPiI2CReadReg8(this->fdImu, BNO055_OPERMODE_REG);
    uint8_t newValue = setBits<uint8_t>(currentValue, BNO055_OPERMODE_MASK, opermode);

    wiringPiI2CWriteReg8(this->fdImu, BNO055_OPERMODE_REG, newValue);

    // Wait for the mode to switch according to the device specs
    switch (opermode) {
        case bno055_opermode_config:
            delay(20);
            break;
        default:
            delay(8);
            break;
    }
}

bno055_accrange_t ImuAccelerometerDriver::getAccelerometerRange(bool skipCache) {
    if (this->hasCachedAccrange && !skipCache) {
        return this->cachedAccrange;
    }

    bno055_accrange_t result = this->getDeviceParameter<bno055_accrange_t>(1, BNO055_ACCRANGE_REG, BNO055_ACCRANGE_MASK, BNO055_ACCRANGE_SHIFT);

    this->cachedAccrange = result;
    this->hasCachedAccrange = true;
    return result;
}

void ImuAccelerometerDriver::setAccelerometerRange(bno055_accrange_t accrange) {
    this->setDeviceParameter(1, BNO055_ACCRANGE_REG, BNO055_ACCRANGE_MASK, BNO055_ACCRANGE_SHIFT, accrange);
    this->cachedAccrange = accrange;
}

bno055_accbandwidth_t ImuAccelerometerDriver::getAccelerometerBandwidth(bool skipCache) {
    if (this->hasCachedAccbandwidth && !skipCache) {
        return this->cachedAccbandwidth;
    }

    bno055_accbandwidth_t result = this->getDeviceParameter<bno055_accbandwidth_t>(1, BNO055_ACCBANDWIDTH_REG, BNO055_ACCBANDWIDTH_MASK, BNO055_ACCBANDWIDTH_SHIFT);

    this->cachedAccbandwidth = result;
    this->hasCachedAccbandwidth = true;
    return result;
}

void ImuAccelerometerDriver::setAccelerometerBandwidth(bno055_accbandwidth_t accbandwidth) {
    this->setDeviceParameter(1, BNO055_ACCBANDWIDTH_REG, BNO055_ACCBANDWIDTH_MASK, BNO055_ACCBANDWIDTH_SHIFT, accbandwidth);
    this->cachedAccbandwidth = accbandwidth;
}

bno055_accopermode_t ImuAccelerometerDriver::getAccelerometerOperationMode(bool skipCache) {
    if (this->hasCachedAccopermode && !skipCache) {
        return this->cachedAccopermode;
    }

    bno055_accopermode_t result = this->getDeviceParameter<bno055_accopermode_t>(1, BNO055_ACCOPERMODE_REG, BNO055_ACCOPERMODE_MASK, BNO055_ACCOPERMODE_SHIFT);

    this->cachedAccopermode = result;
    this->hasCachedAccopermode = true;
    return result;
}

void ImuAccelerometerDriver::setAccelerometerOperationMode(bno055_accopermode_t accopermode) {
    this->setDeviceParameter(1, BNO055_ACCOPERMODE_REG, BNO055_ACCOPERMODE_MASK, BNO055_ACCOPERMODE_SHIFT, accopermode);
    this->cachedAccopermode = accopermode;
}

bno055_gyrorange_t ImuAccelerometerDriver::getGyroscopeRange(bool skipCache) {
    if (this->hasCachedGyrorange && !skipCache) {
        return this->cachedGyrorange;
    }

    bno055_gyrorange_t result = this->getDeviceParameter<bno055_gyrorange_t>(1, BNO055_GYRORANGE_REG, BNO055_GYRORANGE_MASK, BNO055_GYRORANGE_SHIFT);

    this->cachedGyrorange = result;
    this->hasCachedGyrorange = true;
    return result;
}

void ImuAccelerometerDriver::setGyroscopeRange(bno055_gyrorange_t gyrorange) {
    this->setDeviceParameter(1, BNO055_GYRORANGE_REG, BNO055_GYRORANGE_MASK, BNO055_GYRORANGE_SHIFT, gyrorange);
    this->cachedGyrorange = gyrorange;
}

bno055_gyrobandwidth_t ImuAccelerometerDriver::getGyroscopeBandwidth(bool skipCache) {
    if (this->hasCachedGyrobandwidth && !skipCache) {
        return this->cachedGyrobandwidth;
    }

    bno055_gyrobandwidth_t result = this->getDeviceParameter<bno055_gyrobandwidth_t>(1, BNO055_GYROBANDWIDTH_REG, BNO055_GYROBANDWIDTH_MASK, BNO055_GYROBANDWIDTH_SHIFT);

    this->cachedGyrobandwidth = result;
    this->hasCachedGyrobandwidth = true;
    return result;
}

void ImuAccelerometerDriver::setGyroscopeBandwidth(bno055_gyrobandwidth_t gyrobandwidth) {
    this->setDeviceParameter(1, BNO055_GYROBANDWIDTH_REG, BNO055_GYROBANDWIDTH_MASK, BNO055_GYROBANDWIDTH_SHIFT, gyrobandwidth);
    this->cachedGyrobandwidth = gyrobandwidth;
}

bno055_gyroopermode_t ImuAccelerometerDriver::getGyroscopeOperationMode(bool skipCache) {
    if (this->hasCachedGyroopermode && !skipCache) {
        return this->cachedGyroopermode;
    }

    bno055_gyroopermode_t result = this->getDeviceParameter<bno055_gyroopermode_t>(1, BNO055_GYROOPERMODE_REG, BNO055_GYROOPERMODE_MASK, BNO055_GYROOPERMODE_SHIFT);

    this->cachedGyroopermode = result;
    this->hasCachedGyroopermode = true;
    return result;
}

void ImuAccelerometerDriver::setGyroscopeOperationMode(bno055_gyroopermode_t gyroopermode) {
    this->setDeviceParameter(1, BNO055_GYROOPERMODE_REG, BNO055_GYROOPERMODE_MASK, BNO055_GYROOPERMODE_SHIFT, gyroopermode);
    this->cachedGyroopermode = gyroopermode;
}

bno055_magdatarate_t ImuAccelerometerDriver::getMagnetometerDataRage(bool skipCache) {
    if (this->hasCachedMagdatarate && !skipCache) {
        return this->cachedMagdatarate;
    }

    bno055_magdatarate_t result = this->getDeviceParameter<bno055_magdatarate_t>(1, BNO055_MAGDATARATE_REG, BNO055_MAGDATARATE_MASK, BNO055_MAGDATARATE_SHIFT);

    this->cachedMagdatarate = result;
    this->hasCachedMagdatarate = true;
    return result;
}

void ImuAccelerometerDriver::setMagnetometerDataRage(bno055_magdatarate_t magdatarate) {
    this->setDeviceParameter(1, BNO055_MAGDATARATE_REG, BNO055_MAGDATARATE_MASK, BNO055_MAGDATARATE_SHIFT, magdatarate);
    this->cachedMagdatarate = magdatarate;
}

bno055_magopermode_t ImuAccelerometerDriver::getMagnetometerOperationMode(bool skipCache) {
    if (this->hasCachedMagopermode && !skipCache) {
        return this->cachedMagopermode;
    }

    bno055_magopermode_t result = this->getDeviceParameter<bno055_magopermode_t>(1, BNO055_MAGOPERMODE_REG, BNO055_MAGOPERMODE_MASK, BNO055_MAGOPERMODE_SHIFT);

    this->cachedMagopermode = result;
    this->hasCachedMagopermode = true;
    return result;
}

void ImuAccelerometerDriver::setMagnetometerOperationMode(bno055_magopermode_t magopermode) {
    this->setDeviceParameter(1, BNO055_MAGOPERMODE_REG, BNO055_MAGOPERMODE_MASK, BNO055_MAGOPERMODE_SHIFT, magopermode);
    this->cachedMagopermode = magopermode;
}

bno055_magpowermode_t ImuAccelerometerDriver::getMagnetometerPowerMode(bool skipCache) {
    if (this->hasCachedMagpowermode && !skipCache) {
        return this->cachedMagpowermode;
    }

    bno055_magpowermode_t result = this->getDeviceParameter<bno055_magpowermode_t>(1, BNO055_MAGPOWERMODE_REG, BNO055_MAGPOWERMODE_MASK, BNO055_MAGPOWERMODE_SHIFT);

    this->cachedMagpowermode = result;
    this->hasCachedMagpowermode = true;
    return result;
}

void ImuAccelerometerDriver::setMagnetometerPowerMode(bno055_magpowermode_t magpowermode) {
    this->setDeviceParameter(1, BNO055_MAGPOWERMODE_REG, BNO055_MAGPOWERMODE_MASK, BNO055_MAGPOWERMODE_SHIFT, magpowermode);
    this->cachedMagpowermode = magpowermode;
}

bno055_acc_units_t ImuAccelerometerDriver::getAccelerationUnits(bool skipCache) {
    if (this->hasCachedAccUnits && !skipCache) {
        return this->cachedAccUnits;
    }

    bno055_acc_units_t result = this->getDeviceParameter<bno055_acc_units_t>(0, BNO055_ACC_UNITS_REG, BNO055_ACC_UNITS_MASK, BNO055_ACC_UNITS_SHIFT);

    this->cachedAccUnits = result;
    this->hasCachedAccUnits = true;
    return result;
}

void ImuAccelerometerDriver::setAccelerationUnits(bno055_acc_units_t accUnits) {
    this->setDeviceParameter(0, BNO055_ACC_UNITS_REG, BNO055_ACC_UNITS_MASK, BNO055_ACC_UNITS_SHIFT, accUnits);
    this->cachedAccUnits = accUnits;
}

bno055_angrate_units_t ImuAccelerometerDriver::getAngularRateUnits(bool skipCache) {
    if (this->hasCachedAngrateUnits && !skipCache) {
        return this->cachedAngrateUnits;
    }

    bno055_angrate_units_t result = this->getDeviceParameter<bno055_angrate_units_t>(0, BNO055_ANGRATE_UNITS_REG, BNO055_ANGRATE_UNITS_MASK, BNO055_ANGRATE_UNITS_SHIFT);

    this->cachedAngrateUnits = result;
    this->hasCachedAngrateUnits = true;
    return result;
}

void ImuAccelerometerDriver::setAngularRateUnits(bno055_angrate_units_t angrateUnits) {
    this->setDeviceParameter(0, BNO055_ANGRATE_UNITS_REG, BNO055_ANGRATE_UNITS_MASK, BNO055_ANGRATE_UNITS_SHIFT, angrateUnits);
    this->cachedAngrateUnits = angrateUnits;
}

bno055_ang_units_t ImuAccelerometerDriver::getAngleUnits(bool skipCache) {
    if (this->hasCachedAngUnits && !skipCache) {
        return this->cachedAngUnits;
    }

    bno055_ang_units_t result = this->getDeviceParameter<bno055_ang_units_t>(0, BNO055_ANG_UNITS_REG, BNO055_ANG_UNITS_MASK, BNO055_ANG_UNITS_SHIFT);

    this->cachedAngUnits = result;
    this->hasCachedAngUnits = true;
    return result;
}

void ImuAccelerometerDriver::setAngleUnits(bno055_ang_units_t angUnits) {
    this->setDeviceParameter(0, BNO055_ANG_UNITS_REG, BNO055_ANG_UNITS_MASK, BNO055_ANG_UNITS_SHIFT, angUnits);
    this->cachedAngUnits = angUnits;
}

bno055_temp_units_t ImuAccelerometerDriver::getTemperatureUnits(bool skipCache) {
    if (this->hasCachedTempUnits && !skipCache) {
        return this->cachedTempUnits;
    }

    bno055_temp_units_t result = this->getDeviceParameter<bno055_temp_units_t>(0, BNO055_TEMP_UNITS_REG, BNO055_TEMP_UNITS_MASK, BNO055_TEMP_UNITS_SHIFT);

    this->cachedTempUnits = result;
    this->hasCachedTempUnits = true;
    return result;
}

void ImuAccelerometerDriver::setTemperatureUnits(bno055_temp_units_t tempUnits) {
    this->setDeviceParameter(0, BNO055_TEMP_UNITS_REG, BNO055_TEMP_UNITS_MASK, BNO055_TEMP_UNITS_SHIFT, tempUnits);
    this->cachedTempUnits = tempUnits;
}

bno055_data_format_t ImuAccelerometerDriver::getDataFormat(bool skipCache) {
    if (this->hasCachedDataFormat && !skipCache) {
        return this->cachedDataFormat;
    }

    bno055_data_format_t result = this->getDeviceParameter<bno055_data_format_t>(0, BNO055_DATA_FORMAT_REG, BNO055_DATA_FORMAT_MASK, BNO055_DATA_FORMAT_SHIFT);

    this->cachedDataFormat = result;
    this->hasCachedDataFormat = true;
    return result;
}

void ImuAccelerometerDriver::setDataFormat(bno055_data_format_t dataFormat) {
    this->setDeviceParameter(0, BNO055_DATA_FORMAT_REG, BNO055_DATA_FORMAT_MASK, BNO055_DATA_FORMAT_SHIFT, dataFormat);
    this->cachedDataFormat = dataFormat;
}

/* #endregion Parameters */

/* #endregion Public */

/* #region Private */

ImuAccelerometerDriver *ImuAccelerometerDriver::instance;

void ImuAccelerometerDriver::setPage(uint8_t page) {
    page &= 0b1;

    wiringPiI2CWriteReg8(this->fdImu, BNO055_PAGE_ID_REG, page);
}

/* #region LSB Calculation */

double ImuAccelerometerDriver::getAccelerometerLsb() {
    switch (this->getAccelerationUnits()) {
        case bno055_acc_units_mg:
            return BNO055_ACCELERATION_LSB_MG;
        default:
            return BNO055_ACCELERATION_LSB_MS2;
    }
}

double ImuAccelerometerDriver::getGyrometerLsb() {
    switch (this->getAngularRateUnits()) {
        case bno055_angrate_units_rps:
            return BNO055_GYROSCOPE_LSB_RPS;
        default:
            return BNO055_GYROSCOPE_LSB_DPS;
    }
}

double ImuAccelerometerDriver::getTemperatureLsb() {
    switch (this->getTemperatureUnits()) {
        case bno055_temp_units_fahrenheit:
            return BNO055_TEMPERATURE_LSB_F;
        default:
            return BNO055_TEMPERATURE_LSB_C;
    }
}

double ImuAccelerometerDriver::getEulerOrientationLsb() {
    switch (this->getAngleUnits()) {
        case bno055_ang_units_rad:
            return BNO055_ORIENT_EULER_LSB_RAD;
        default:
            return BNO055_ORIENT_EULER_LSB_DEG;
    }
}

/* #endregion LSB Calculation */

void ImuAccelerometerDriver::readVectors(bno055_vector_t **data, uint8_t numVectors, uint8_t startingRegister, const double *lsbs) {
    // Select register
    wiringPiI2CWrite(this->fdImu, startingRegister);

    int16_t buffer[3 * numVectors];

    // Read 3 * numVectors signed 16-bit integers directly into our buffer
    read(this->fdImu, &buffer, sizeof(int16_t) * 3 * numVectors);

    // Populate structs
    for (int vec = 0; vec < numVectors; vec++) {
        bno055_vector_t *vector = data[vec];
        double lsb = lsbs[vec];

        // Copy raw values from buffer into vector
        memcpy(vector, &buffer[vec * 3], sizeof(int16_t) * 3);

        // Calculate scalar values from raw values
        vector->x = ((double) vector->rawX) / lsb;
        vector->y = ((double) vector->rawY) / lsb;
        vector->z = ((double) vector->rawZ) / lsb;
    }
}

void ImuAccelerometerDriver::readVector(bno055_vector_t &data, uint8_t startingRegister, double lsb) {
    // Select register
    wiringPiI2CWrite(this->fdImu, startingRegister);

    // Read 3 signed 16-bit integers directly into our struct
    read(this->fdImu, &data, sizeof(int16_t) * 3);

    data.x = ((double) data.rawX) / lsb;
    data.y = ((double) data.rawY) / lsb;
    data.z = ((double) data.rawZ) / lsb;
}

void ImuAccelerometerDriver::readQuaternion(bno055_quaternion_t &data, uint8_t startingRegister) {
    // Select register
    wiringPiI2CWrite(this->fdImu, startingRegister);

    // Read 4 signed 16-bit integers directly into our struct
    read(this->fdImu, &data, sizeof(int16_t) * 4);

    const double lsb = BNO055_QUATERNION_LSB;

    data.w = ((double) data.rawW) / lsb;
    data.x = ((double) data.rawX) / lsb;
    data.y = ((double) data.rawY) / lsb;
    data.z = ((double) data.rawZ) / lsb;
}

/* #endregion Private */