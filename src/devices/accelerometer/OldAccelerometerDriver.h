#pragma once

#include <stdint.h>

#include "AccelerometerDriver.h"

#define GRAVITY_EARTH       9.806

#define I2C_REG_CTRL_1      0x20
#define I2C_REG_CTRL_2      0x21
#define I2C_REG_CTRL_3      0x22
#define I2C_REG_CTRL_4      0x23
#define I2C_REG_STATUS      0x27
#define I2C_REG_X_L         0x28
#define I2C_REG_X_H         0x29
#define I2C_REG_Y_L         0x2A
#define I2C_REG_Y_H         0x2B
#define I2C_REG_Z_L         0x2C
#define I2C_REG_Z_H         0x2D

class OldAccelerometerDriver : public AccelerometerDriver {
public:
    OldAccelerometerDriver(uint8_t i2cAddress);
    ~OldAccelerometerDriver();

    void initialize();
    void getOrientation(angles_t &data);
    void getAngularVelocity(angles_t &data);
    void getAcceleration(vector_t &data);

private:
    uint8_t i2cAddress = 0;
    int fdAccelerometer = -1;

    bool updateRaw(int16_t &x, int16_t &y, int16_t &z);
    void setDeviceMode();
};