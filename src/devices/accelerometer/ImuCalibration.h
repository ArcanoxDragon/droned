#pragma once

#include <stdint.h>

struct __attribute__((packed)) bno055_calibration_t {
    int16_t accOffsetX, accOffsetY, accOffsetZ;
    int16_t magOffsetX, magOffsetY, magOffsetZ;
    int16_t gyroOffsetX, gyroOffsetY, gyroOffsetZ;
    int16_t accRadius, magRadius;
};

class ImuCalibration {
public:
    static bool savedCalibrationExists();
    static void deleteCalibration();
    static bool loadSavedCalibration(bno055_calibration_t &data);
    static bool saveCalibration(const bno055_calibration_t &data);
};