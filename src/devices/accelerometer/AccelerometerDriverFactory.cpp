#include <stdexcept>

#include "../../config.h"
#include "../../ErrorMessage.h"
#include "AccelerometerDriverFactory.h"
#include "OldAccelerometerDriver.h"
#include "ImuAccelerometerDriver.h"

using stringstream = std::stringstream;

/* #region Public */

AccelerometerDriver *AccelerometerDriverFactory::createDriver(const string name) {
    if (name == "old") {
        return new OldAccelerometerDriver(I2C_ADDR_ACCELEROMETER);
    } else if (name == "imu") {
        return new ImuAccelerometerDriver(I2C_ADDR_IMU);
    }

    throw std::runtime_error(ErrorMessage() << "Error: no accelerometer driver named \"" << name << "\"");
}

/* #endregion Public */