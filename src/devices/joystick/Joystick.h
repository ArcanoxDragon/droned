#pragma once

#include <stdint.h>

#include "JoystickSetter.h"

#define JOY_BUTTONS_SUPPORTED       24

class JoystickDriver;

class Joystick {
public:
    static void initialize();
    static void initialize(JoystickDriver *driver);
    static void dispose();
    static bool isConnected();

    static double getX1();
    static double getY1();
    static double getX2();
    static double getY2();
    static bool getButton(uint8_t button);
    static bool getButtonOneShot(uint8_t button);

private:
    class Setter : public JoystickSetter {
    public:
        void setX1(double value);
        void setY1(double value);
        void setX2(double value);
        void setY2(double value);
        void setButton(uint8_t button, bool value);
    };

    static JoystickDriver *driver;
    static JoystickSetter *setter;
    static double x1, y1, x2, y2;
    static bool buttons[];
    static bool buttonOneShots[];
};