#pragma once

#define JOY_AXIS_X1         0
#define JOY_AXIS_Y1         1
#define JOY_AXIS_X2         2
#define JOY_AXIS_Y2         3

#define JOY_BUTTON_B        0
#define JOY_BUTTON_A        1
#define JOY_BUTTON_Y        2
#define JOY_BUTTON_X        3
#define JOY_BUTTON_L        4
#define JOY_BUTTON_R        5
#define JOY_BUTTON_ZL       6
#define JOY_BUTTON_ZR       7
#define JOY_BUTTON_MINUS    8
#define JOY_BUTTON_PLUS     9
#define JOY_BUTTON_LSTICK   10
#define JOY_BUTTON_RSTICK   11
#define JOY_BUTTON_HOME     12
#define JOY_BUTTON_SHARE    13