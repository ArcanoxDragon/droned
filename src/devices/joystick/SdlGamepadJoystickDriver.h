#pragma once

#include <SDL2/SDL.h>

#include "../../events/EventBus.h"
#include "JoystickDriver.h"
#include "JoyMappings_SwitchProController.h"

// #define __JOY_DBG

#ifdef __JOY_DBG
    #define __joyprintf(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
    #define __joyprintf(fmt, ...)
#endif

#define JOY_AXIS_MAX        32767.0
#define JOY_AXIS_DEADZONE   0.1
#define JOY_AXIS_RANGE      (1.0 - JOY_AXIS_DEADZONE)

class SdlGamepadJoystickDriver: public JoystickDriver, public EventHandler {
public:
    SdlGamepadJoystickDriver(int8_t joyIndex);
    ~SdlGamepadJoystickDriver();

    void initialize(JoystickSetter *setter) override;
    bool isConnected() override;
    void handleEvent(const SDL_Event *event);
    
private:
    SDL_Joystick *joystick = NULL;
    SDL_JoystickID joyId = 0;
    SDL_JoystickGUID initialJoyGuid = {};
    bool gotInitialDevice = false;
    int8_t initialDeviceWanted = -1;
    
    void attachJoystick(SDL_Joystick *joystick);
    void handleJoyAxisEvent(const SDL_Event *event);
    void handleJoyButtonEvent(const SDL_Event *event, bool wasDown);
    void handleDeviceAddedEvent(const SDL_Event *event);
    void handleDeviceRemovedEvent(const SDL_Event *event);
};