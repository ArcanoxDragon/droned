#include <stddef.h>
#include <string>

#include "../../output.h"
#include "../../settings.h"
#include "Joystick.h"
#include "JoystickDriver.h"
#include "JoystickDriverFactory.h"

using string = std::string;

/* #region Public */

void Joystick::initialize() {
    json settings = Settings::get();
    string driverName = settings["joystickDriver"].get<string>();

    for (uint8_t button = 0; button < JOY_BUTTONS_SUPPORTED; button++) {
        buttons[button] = false;
        buttonOneShots[button] = false;
    }

    initialize(JoystickDriverFactory::createDriver(driverName));
}

void Joystick::initialize(JoystickDriver *driver) {
    LOG_VERBOSE("Initializing Joystick...\n");

    // Dispose before initializing in case we had been initialized before
    dispose();

    Joystick::driver = driver;
    Joystick::setter = new Setter();

    driver->initialize(Joystick::setter);
}

void Joystick::dispose() {
    if (driver) {
        // "initialize" may be called with an inline new object, or using the factory, so we should
        // clean up the driver instance here
        delete driver;
        driver = NULL;
    }

    if (setter) {
        delete setter;
        setter = NULL;
    }
}

bool Joystick::isConnected() {
    if (!driver) return false;
    
    return driver->isConnected();
}

double Joystick::getX1() {
    return x1;
}

double Joystick::getY1() {
    return y1;
}

double Joystick::getX2() {
    return x2;
}

double Joystick::getY2() {
    return y2;
}

bool Joystick::getButton(uint8_t button) {
    if (button >= JOY_BUTTONS_SUPPORTED) return false;

    return buttons[button];
}

bool Joystick::getButtonOneShot(uint8_t button) {
    if (button >= JOY_BUTTONS_SUPPORTED) return false;

    if (buttons[button]) {
        if (!buttonOneShots[button]) {
            buttonOneShots[button] = true;
            return true;
        }
    }
    else {
        buttonOneShots[button] = false;
    }

    return false;
}

/* #endregion Public */

/* #region Private */

JoystickDriver *Joystick::driver = NULL;
JoystickSetter *Joystick::setter = NULL;
double Joystick::x1 = 0.0;
double Joystick::y1 = 0.0;
double Joystick::x2 = 0.0;
double Joystick::y2 = 0.0;
bool Joystick::buttons[JOY_BUTTONS_SUPPORTED];
bool Joystick::buttonOneShots[JOY_BUTTONS_SUPPORTED];

void Joystick::Setter::setX1(double value) {
    Joystick::x1 = value;
}

void Joystick::Setter::setY1(double value) {
    Joystick::y1 = value;
}

void Joystick::Setter::setX2(double value) {
    Joystick::x2 = value;
}

void Joystick::Setter::setY2(double value) {
    Joystick::y2 = value;
}

void Joystick::Setter::setButton(uint8_t button, bool value) {
    if (button >= JOY_BUTTONS_SUPPORTED) return;

    buttons[button] = value;
    buttonOneShots[button] &= value;
}

/* #endregion Private */