#pragma once

#include <stddef.h>

#include "JoystickSetter.h"

class JoystickDriver {
public:
    virtual ~JoystickDriver() { }

    virtual void initialize(JoystickSetter *setter);
    virtual bool isConnected() = 0;

protected:
    JoystickDriver() { }

    JoystickSetter *getSetter();

private:
    JoystickSetter *setter = NULL;
};