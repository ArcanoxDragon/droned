#pragma once

#include <stdint.h>

class JoystickSetter {
public:
    virtual void setX1(double value) = 0;
    virtual void setY1(double value) = 0;
    virtual void setX2(double value) = 0;
    virtual void setY2(double value) = 0;
    virtual void setButton(uint8_t button, bool value) = 0;
};