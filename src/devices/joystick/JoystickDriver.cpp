#include "Joystick.h"
#include "JoystickDriver.h"

/* #region Public */

void JoystickDriver::initialize(JoystickSetter *setter) {
    this->setter = setter;
}

/* #endregion Public */

/* #region Protected */

JoystickSetter *JoystickDriver::getSetter() {
    return this->setter;
}

/* #endregion Protected */