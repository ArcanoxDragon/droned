#include <stdexcept>

#include "../../config.h"
#include "../../settings.h"
#include "../../ErrorMessage.h"
#include "JoystickDriverFactory.h"
#include "SdlGamepadJoystickDriver.h"

using stringstream = std::stringstream;

/* #region Public */

JoystickDriver *JoystickDriverFactory::createDriver(const string name) {
    if (name == "sdlGamepad") {
        json settings = Settings::get();
        int joyIndex = settings["joystick"]["index"].get<int>();

        return new SdlGamepadJoystickDriver(joyIndex);
    }

    throw std::runtime_error(ErrorMessage() << "Error: no joystick driver named \"" << name << "\"");
}

/* #endregion Public */