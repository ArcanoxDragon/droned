#pragma once

#include <string>

#include "JoystickDriver.h"

using string = std::string;

class JoystickDriverFactory {
public:
    static JoystickDriver *createDriver(const string name);
};