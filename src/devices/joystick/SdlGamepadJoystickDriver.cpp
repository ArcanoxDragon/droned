#include <math.h>
#include <SDL2/SDL.h>

#include "../../events/EventBus.h"
#include "../../mathutil.h"
#include "../../output.h"
#include "../../utils.h"
#include "Joystick.h"
#include "SdlGamepadJoystickDriver.h"

inline double scaleAxis(int16_t raw) {
    double normalized = (double) raw / JOY_AXIS_MAX;

    if (normalized >= JOY_AXIS_DEADZONE)
        return (normalized - JOY_AXIS_DEADZONE) / JOY_AXIS_RANGE;
    if (normalized <= -JOY_AXIS_DEADZONE)
        return (normalized + JOY_AXIS_DEADZONE) / JOY_AXIS_RANGE;

    return 0.0;
}

/* #region Public */

SdlGamepadJoystickDriver::SdlGamepadJoystickDriver(int8_t joyIndex) :
    JoystickDriver(),
    initialDeviceWanted(joyIndex) {
    EventBus::get()->listen(this);
}

SdlGamepadJoystickDriver::~SdlGamepadJoystickDriver() {
    if (!this->joystick) return;

    EventBus::get()->unlisten(this);
    SDL_JoystickClose(this->joystick);

    this->joystick = NULL;
}

void SdlGamepadJoystickDriver::initialize(JoystickSetter *setter) {
    if (this->joystick) {
        throw "Joystick is already initialized";
    }

    if (this->initialDeviceWanted >= 0) {
        LOG_VERBOSE("Initializing SDL gamepad joystick driver for joystick %d...\n", this->initialDeviceWanted);
    } else {
        LOG_VERBOSE("Initializing SDL gamepad joystick driver...\n");
    }

    JoystickDriver::initialize(setter);

    if (this->initialDeviceWanted >= 0) {
        if (SDL_NumJoysticks() > this->initialDeviceWanted) {
            SDL_Joystick *joystick = SDL_JoystickOpen(this->initialDeviceWanted);

            if (!joystick) {
                return;
            }

            this->attachJoystick(joystick);
            this->gotInitialDevice = true;
        }
    }
}

bool SdlGamepadJoystickDriver::isConnected() {
    return this->joystick != NULL;
}

void SdlGamepadJoystickDriver::handleEvent(const SDL_Event *event) {
    switch (event->type) {
        case SDL_JOYAXISMOTION:
            if (event->jaxis.which == joyId)
                this->handleJoyAxisEvent(event);
            break;
        case SDL_JOYBUTTONDOWN:
        case SDL_JOYBUTTONUP:
            if (event->jbutton.which == joyId)
                this->handleJoyButtonEvent(event, event->type == SDL_JOYBUTTONDOWN);
            break;
        case SDL_JOYDEVICEADDED:
            this->handleDeviceAddedEvent(event);
            break;
        case SDL_JOYDEVICEREMOVED:
            this->handleDeviceRemovedEvent(event);
            break;
    }
}

/* #endregion Public */

/* #region Private */

void SdlGamepadJoystickDriver::attachJoystick(SDL_Joystick *joystick) {
    this->joystick = joystick;
    this->joyId = SDL_JoystickInstanceID(joystick);

    __joyprintf("Attaching to joystick %i\n", joyId);

    if (!this->gotInitialDevice) {
        this->initialJoyGuid = SDL_JoystickGetGUID(joystick);
    }

    SDL_JoystickSetPlayerIndex(joystick, 0);
}

void SdlGamepadJoystickDriver::handleJoyAxisEvent(const SDL_Event *event) {
    double axisValue = scaleAxis(event->jaxis.value);

    __joyprintf("Axis event: axis %i = %4.2f\n", event->jaxis.axis, axisValue);

    switch (event->jaxis.axis) {
        case JOY_AXIS_X1:
            this->getSetter()->setX1(axisValue);
            break;
        case JOY_AXIS_Y1:
            this->getSetter()->setY1(axisValue);
            break;
        case JOY_AXIS_X2:
            this->getSetter()->setX2(axisValue);
            break;
        case JOY_AXIS_Y2:
            this->getSetter()->setY2(axisValue);
            break;
    }
}

void SdlGamepadJoystickDriver::handleJoyButtonEvent(const SDL_Event *event, bool wasDown) {
    this->getSetter()->setButton(event->jbutton.button, wasDown);
}

void SdlGamepadJoystickDriver::handleDeviceAddedEvent(const SDL_Event *event) {
    SDL_JoystickGUID addedGuid = SDL_JoystickGetDeviceGUID(event->jdevice.which);
    bool attach = false;

    __joyprintf("Device added: %i. ", event->jdevice.which);

    if (gotInitialDevice) {
        __joyprintf("Checking GUID, ");

        if (guidEquals(this->initialJoyGuid.data, addedGuid.data)) {
            __joyprintf("matches.\n");
            attach = true;
        } else {
            __joyprintf("does not match.\n");
        }
    } else {
        __joyprintf("Checking index, ");

        if (event->jdevice.which == this->initialDeviceWanted) {
            __joyprintf("matches.\n");
            attach = true;
        } else {
            __joyprintf("does not match.\n");
        }
    }

    if (attach) {
        __joyprintf("Attaching to new device.\n");

        SDL_Joystick *joystick = SDL_JoystickOpen(event->jdevice.which);

        if (!joystick) {
            return;
        }

        this->attachJoystick(joystick);
    } else {
        __joyprintf("Skipping new device.\n");
    }
}

void SdlGamepadJoystickDriver::handleDeviceRemovedEvent(const SDL_Event *event) {
    __joyprintf("Device removed: %i\n", event->jdevice.which);

    if (event->jdevice.which == this->joyId) {
        SDL_JoystickClose(this->joystick);
        this->joyId = -1;
        this->joystick = NULL;
        
        JoystickSetter *setter = this->getSetter();
        
        // Clear out axis values to 0.0
        setter->setX1(0.0);
        setter->setY1(0.0);
        setter->setX2(0.0);
        setter->setY2(0.0);
        
        // Clear out button values to false
        for (int button = 0; button < JOY_BUTTONS_SUPPORTED; button) {
            setter->setButton(button, false);
        }
    }
}

/* #endregion Private */