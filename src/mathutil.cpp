#include <stdio.h>

#include "mathutil.h"

double map(double val, double srcMin, double srcMax, double dstMin, double dstMax, bool clampOutput) {
    double normalized = (val - srcMin) / (srcMax - srcMin);
    double result = normalized * (dstMax - dstMin) + dstMin;
    
    return clampOutput ? clamp(result, dstMin, dstMax) : result;
}