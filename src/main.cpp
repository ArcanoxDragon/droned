#include <chrono>
#include <limits.h>
#include <stdlib.h>
#include <stdexcept>
#include <thread>
#include <wiringPi.h>
#include <SDL2/SDL.h>

#include "config.h"
#include "devices/accelerometer/Accelerometer.h"
#include "devices/accelerometer/OldAccelerometerDriver.h"
#include "devices/motors/Motors.h"
#include "devices/motors/Pca9685MotorDriver.h"
#include "events/user_events.h"
#include "events/EventBus.h"
#include "ipc/UdpSocket.h"
#include "devices/joystick/Joystick.h"
#include "devices/joystick/SdlGamepadJoystickDriver.h"
#include "output.h"
#include "programs/Program.h"
#include "programs/CalibrateImuProgram.h"
#include "programs/CalibrateOffsetProgram.h"
#include "programs/CalibrateEscsProgram.h"
#include "programs/ImuTestProgram.h"
#include "programs/IpcTestProgram.h"
#include "programs/KillProgram.h"
#include "programs/MainProgram.h"
#include "settings.h"
#include "terminal.h"

using namespace std::chrono_literals;
namespace chrono = std::chrono;
namespace this_thread = std::this_thread;

#define DEFAULT_INIT_FLAGS SDL_INIT_JOYSTICK

void printUsage();
Program *parseArgs(int argc, char *argv[]);
bool parseIntArg(const char *argName, const char *arg, int &result, int min = INT_MIN, int max = INT_MAX);
void processEvent(const SDL_Event *event);

bool running;
SDL_Event event;

int main(int argc, char *argv[]) {
    // Initialize the event bus as the first thing so that stuff can subscribe in constructors
    EventBus::initialize();

    // Determine which program to run
    Program *program = parseArgs(argc, argv);

    if (!program) {
        LOG_VERBOSE("Starting MainProgram...\n");
        program = new MainProgram();
    }

    try {
        // Load config file
        Settings::load();

        // Initialize SDL
        uint32_t sdlInitFlags = DEFAULT_INIT_FLAGS | program->getInitFlags();

        if (SDL_InitSubSystem(sdlInitFlags) < 0) {
            throw SDL_GetError();
        }

        // Initialize user events
        registerUserEvents();

        // Initialize IPC socket
        UdpSocket::initialize();

        // Initialize wiringPi
        wiringPiSetup();

        // Initialize drivers
        Accelerometer::initialize();
        Joystick::initialize();
        Motors::initialize();

        // Initialize program
        program->setup();
    } catch (std::runtime_error &error) {
        fprintf(stderr, "Error during initialization: %s\n", error.what());
        return 1;
    } catch (const char *error) {
        fprintf(stderr, "Error during initialization: %s\n", error);
        return 1;
    }

    // Run the event loop
    running = true;

    while (running) {
        try {
            while (SDL_PollEvent(&event)) {
                processEvent(&event);
            }

            running &= program->loop();
        } catch (std::runtime_error &error) {
            fprintf(stderr, "Caught an error in run loop: %s\n", error.what());
        } catch (const char *error) {
            fprintf(stderr, "Caught an error in run loop: %s\n", error);
        }
    }

    // Clean up program
    delete program;

    // Clean up drivers
    Accelerometer::dispose();
    Joystick::dispose();
    Motors::dispose();

    // Clean up IPC socket
    UdpSocket::dispose();

    // Clean up SDL
    SDL_Quit();

    // Clean up EventBus
    EventBus::dispose();

    // Enable echo in case it was disabled somewhere
    enableEcho();
    printf("\n");

    return 0;
}

void printUsage() {
    printf("Usage: droned [ command ]\n");
    printf("  Available commands:\n");
    printf("    --help                      Print this help message\n");
    printf("    --kill                      Kills all motors by setting the throttles to 0%%, then exits\n");
    printf("    --test-ipc                  Test IPC connection\n");
    printf("    --test-imu                  Tests the inertial measurement unit\n");
    printf("    --calibrate-imu             Calibrates the inertial measurement unit\n");
    printf("    --calibrate-orientation     Calibrates the orientation offset of the drone\n");
    printf("    --calibrate-esc [motor]     Calibrate a motor's ESC module.\n");
    printf("                                If \"motor\" is omitted, all motors are calibrated at once.\n");
    printf("    --test-esc [motor]          Test a motor's ESC module.\n");
    printf("                                If \"motor\" is omitted, all motors are tested at once.\n");
}

Program *parseArgs(int argc, char *argv[]) {
    if (argc >= 2) {
        if (strcmp(argv[1], "--help") == 0) {
            printUsage();
            exit(0);
        } else if (strcmp(argv[1], "--kill") == 0) {
            return new KillProgram();
        } else if (strcmp(argv[1], "--test-ipc") == 0) {
            return new IpcTestProgram();
        } else if (strcmp(argv[1], "--test-imu") == 0) {
            return new ImuTestProgram();
        } else if (strcmp(argv[1], "--calibrate-imu") == 0) {
            return new CalibrateImuProgram();
        } else if (strcmp(argv[1], "--calibrate-orientation") == 0) {
            return new CalibrateOffsetProgram();
        } else if (strcmp(argv[1], "--calibrate-esc") == 0 || strcmp(argv[1], "--test-esc") == 0) {
            bool testOnly = strcmp(argv[1], "--test-esc") == 0;

            if (argc >= 3) {
                int motorIndex;

                if (parseIntArg("motor", argv[2], motorIndex, 0, 3)) {
                    LOG_VERBOSE("Starting ConfigureEscsProgram for motor %d...\n", motorIndex);
                    return new CalibrateEscsProgram(motorIndex, testOnly);
                }
            } else {
                LOG_VERBOSE("Starting ConfigureEscsProgram for all motors...\n");
                return new CalibrateEscsProgram(-1, testOnly);
            }
        } else {
            printUsage();
            exit(1);
        }
    }

    return NULL;
}

bool parseIntArg(const char *argName, const char *arg, int &result, int min, int max) {
    char *endPtr;
    int convertResult = strtol(arg, &endPtr, 10);
    bool success = false;

    if (*endPtr == '\0' && errno != ERANGE) {
        if (min != INT_MIN && max != INT_MAX) {
            success = convertResult >= min && convertResult <= max;
        } else if (min != INT_MIN) {
            success = convertResult >= min;
        } else if (max != INT_MAX) {
            success = convertResult <= max;
        } else {
            success = true;
        }
    }

    if (success) {
        result = convertResult;
    } else {
        if (min != INT_MIN && max != INT_MAX) {
            fprintf(stderr, "%s must be a number between %d and %d\n\n", argName, min, max);
        } else if (min != INT_MIN) {
            fprintf(stderr, "%s must be a number greater than %d\n\n", argName, min);
        } else if (max != INT_MAX) {
            fprintf(stderr, "%s must be a number less than %d\n\n", argName, max);
        } else {
            fprintf(stderr, "%s must be a number\n\n", argName);
        }

        printUsage();
        exit(1);
    }

    return success;
}

void processEvent(const SDL_Event *event) {
    switch (event->type) {
        case SDL_QUIT: {
            running = false;
            break;
        }
        default: {
            EventBus::get()->post(event);

            // If the event was a user event, see if there is data in either of the pointers and free it if so.
            // Doing this here instead of somewhere else means multiple event handlers can access the data without
            // having to know it's the last one, and the data is guaranteed to be freed.
            if (isUserEvent(event->type)) {
                if (event->user.data1) {
                    free(event->user.data1);
                }

                if (event->user.data2) {
                    free(event->user.data2);
                }
            }

            break;
        }
    }
}
