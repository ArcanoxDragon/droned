#pragma once

void clearTerm();
void clearLine(bool resetCursor = false);
void cursorPos(int x, int y);
bool isEchoEnabled();
bool disableEcho();
bool enableEcho();
bool charAvailable();