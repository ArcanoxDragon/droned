#pragma once

#include <string>
#include <sstream>

class ErrorMessage {
public:
    ErrorMessage() { }
    ~ErrorMessage() { }

    template <typename Type>
    ErrorMessage &operator << (const Type &value) {
        this->stream << value;
        return *this;
    }

    std::string str() const;
    operator std::string() const;

private:
    std::stringstream stream;

    ErrorMessage(const ErrorMessage &other);
    ErrorMessage &operator =(ErrorMessage &other);
};