#pragma once

class TemporalSmoother {
public:
    TemporalSmoother(double rate) : inRate(rate), outRate(rate) { }
    TemporalSmoother(double inRate, double outRate) : inRate(inRate), outRate(outRate) { }

    double get(double x, double dt);
    void reset();

private:
    double inRate = 1.0;
    double outRate = 1.0;
    double state = 0.0;
    double diff = 0.0;
};