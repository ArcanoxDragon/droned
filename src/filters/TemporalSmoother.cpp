#include <algorithm>

#include "TemporalSmoother.h"

double TemporalSmoother::get(double x, double dt) {
    this->diff = x - this->state;
    
    double rate = (this->diff * this->state) >= 0 ? this->outRate : this->inRate;
    double timeRate = rate * dt;
    
    this->state += this->diff * timeRate / (1 + timeRate);
    
    return this->state;
}

void TemporalSmoother::reset() {
    this->state = 0.0;
}