#include <algorithm>
#include <cstring>

#include "MovingAverage.h"

/* #region Public */

double MovingAverage::get(double sample) {
    int currentBucket = this->nextBucket++;

    this->nextBucket %= this->windowSize;
    this->state += (sample - this->samples[currentBucket]) / (double) this->windowSize;
    this->samples[currentBucket] = sample;

    return this->state;
}

double MovingAverage::peek() {
    return this->state;
}

void MovingAverage::reset() {
    for (int bucket = 0; bucket < this->windowSize; bucket++) {
        this->samples[bucket] = 0.0;
    }

    this->nextBucket = 0;
}

void MovingAverage::setWindowSize(int newWindowSize) {
    double *newSamples = new double[newWindowSize];

    // Copy as much of the old bucket as we can to the new bucket
    memcpy(newSamples, this->samples, sizeof(double) * std::min(this->windowSize, newWindowSize));

    // If the new bucket is larger, fill the remaining values with the current average so the resulting average doesn't change
    if (newWindowSize > this->windowSize) {
        for (int i = this->windowSize; i < newWindowSize; i++) {
            newSamples[i] = this->state;
        }
    }
    
    // Free up our old bucket and swap in the new one
    delete this->samples;
    this->samples = newSamples;
}

int MovingAverage::getWindowSize() {
    return this->windowSize;
}

/* #endregion Public */