#pragma once

#define MA_DEFAULT_WINDOW_SIZE      10

class MovingAverage {
public:
    MovingAverage() : MovingAverage(MA_DEFAULT_WINDOW_SIZE) { }

    MovingAverage(int windowSize) :windowSize(windowSize) {
        samples = new double[windowSize];
    }

    ~MovingAverage() {
        delete samples;
    }
    
    double get(double sample);
    double peek();
    void reset();
    
    void setWindowSize(int newWindowSize);
    int getWindowSize();

private:
    double *samples;
    double state = 0.0;
    int windowSize;
    int nextBucket = 0;
};