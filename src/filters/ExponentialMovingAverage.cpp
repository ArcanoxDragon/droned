#include "../mathutil.h"
#include "ExponentialMovingAverage.h"

/* #region Public */

ExponentialMovingAverage::ExponentialMovingAverage(double alpha) { 
    this->setAlpha(alpha);
}

ExponentialMovingAverage::~ExponentialMovingAverage() { }

double ExponentialMovingAverage::getAlpha() {
    return this->alpha;
}

void ExponentialMovingAverage::setAlpha(double alpha) {
    this->alpha = clamp(alpha, 0.0, 1.0);
}

double ExponentialMovingAverage::get(double sample) {
    double currentValue = this->value;
    
    this->value = this->alpha * sample + (1.0 - this->alpha) * this->lastValue;
    this->lastValue = currentValue;
    
    return this->value;
}

double ExponentialMovingAverage::peek() {
    return this->value;
}

void ExponentialMovingAverage::reset(double value) {
    this->value = value;
    this->lastValue = value;
}

/* #endregion Public */