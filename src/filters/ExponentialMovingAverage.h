#pragma once

class ExponentialMovingAverage {
public:
    ExponentialMovingAverage(double alpha);
    ~ExponentialMovingAverage();

    double getAlpha();
    void setAlpha(double alpha);
    double get(double sample);
    double peek();
    void reset(double value = 0.0);

private:
    double alpha = 0.0;
    double value = 0.0;
    double lastValue = 0.0;
};