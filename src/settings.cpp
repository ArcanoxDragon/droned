#include <fstream>
#include <stdexcept>

#include "settings.h"
#include "ErrorMessage.h"

using ifstream = std::ifstream;
using ofstream = std::ofstream;
using ios = std::ios;

/* #region Public */

void Settings::load() {
    if (loaded) return;

    ifstream settingsStream(SETTINGS_PATH);

    if (settingsStream.good()) {
        // File exists, read it

        try {
            settingsStream >> instance;
            loaded = true;
        } catch (nlohmann::detail::exception &exception) {
            throw std::runtime_error(ErrorMessage() << "JSON error: " << exception.what());
        }
    } else {
        // File doesn't exist, create it

        settingsStream.close();
        instance = createDefault();

        ofstream defaultStream(SETTINGS_PATH, ios::trunc);

        if (!defaultStream.good()) {
            throw std::runtime_error(ErrorMessage() << "Could not create default settings file: " << SETTINGS_PATH);
        }

        defaultStream << instance.dump(4);
        loaded = true;
    }
}

json Settings::get() {
    if (!loaded) {
        throw "Cannot get settings instance before it has been loaded";
    }

    return instance;
}

/* #endregion Public */

/* #region Private */

bool Settings::loaded = false;
json Settings::instance;

json Settings::createDefault() {
    json j;

    j["accelerometerDriver"] = "imu";
    j["motorDriver"] = "pca9685";
    j["joystickDriver"] = "sdlGamepad";
    j["joystick"]["index"] = 0;

    return j;
}

/* #endregion Private */