#pragma once

#define datastruct struct __attribute__((packed))

datastruct angles_t {
    double pitch;
    double roll;
    double heading;
};

datastruct vector_t {
    double x;
    double y;
    double z;
};