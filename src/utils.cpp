#include <limits>
#include <iostream>

#include "utils.h"
#include "terminal.h"

bool guidEquals(uint8_t const first[], uint8_t const second[]) {
    for (int i = 0; i < GUID_LENGTH; i++) {
        if (first[i] != second[i])
            return false;
    }

    return true;
}

void waitForEnterKey() {
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

bool testForEnterKey() {
    while (charAvailable()) {
        int chr = getchar();

        if (chr == '\n') {
            return true;
        }
    }

    return false;
}