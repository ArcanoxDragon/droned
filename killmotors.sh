#!/bin/bash

I2C_ADDRESS=0x40

# Put PWM chip to sleep so we can set the frequency
i2cset -y 1 I2C_ADDRESS 0x00 0x10

# Write PWM prescale value of 122
i2cset -y 1 I2C_ADDRESS 0xFE 122

# Write 0 to the "ON" time for all 4 motor channels
i2cset -y 1 I2C_ADDRESS 0x06 0    # channel 0, on LSB
i2cset -y 1 I2C_ADDRESS 0x07 0    # channel 0, on MSB
i2cset -y 1 I2C_ADDRESS 0x0A 0    # channel 1, on LSB
i2cset -y 1 I2C_ADDRESS 0x0B 0    # channel 1, on MSB
i2cset -y 1 I2C_ADDRESS 0x0E 0    # channel 2, on LSB
i2cset -y 1 I2C_ADDRESS 0x0F 0    # channel 2, on MSB
i2cset -y 1 I2C_ADDRESS 0x12 0    # channel 3, on LSB
i2cset -y 1 I2C_ADDRESS 0x13 0    # channel 3, on MSB

# Write 204 to the "OFF" time for all 4 motor channels (results in a 1000us or 1ms pulse width)
i2cset -y 1 I2C_ADDRESS 0x08 204  # channel 0, off LSB
i2cset -y 1 I2C_ADDRESS 0x09 0    # channel 0, off MSB
i2cset -y 1 I2C_ADDRESS 0x0C 204  # channel 1, off LSB
i2cset -y 1 I2C_ADDRESS 0x0D 0    # channel 1, off MSB
i2cset -y 1 I2C_ADDRESS 0x10 204  # channel 2, off LSB
i2cset -y 1 I2C_ADDRESS 0x11 0    # channel 2, off MSB
i2cset -y 1 I2C_ADDRESS 0x14 204  # channel 3, off LSB
i2cset -y 1 I2C_ADDRESS 0x15 0    # channel 3, off MSB

# Wake up the PWM chip so it starts emitting a 0-throttle signal
i2cset -y 1 I2C_ADDRESS 0x00 0x00
